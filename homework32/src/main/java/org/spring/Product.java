package org.spring;

import lombok.*;

import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter @Setter @ToString
public class Product {

    @Setter(AccessLevel.NONE)
    private Long id;
    private String name;
    private double price;

    private static AtomicLong idCounter = new AtomicLong();

    public Product(@NonNull String name, double price) {
        if (price < 0) throw new IllegalArgumentException();
        this.id = idCounter.incrementAndGet();
        this.name = name;
        this.price = price;
    }
}
