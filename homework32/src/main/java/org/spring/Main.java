package org.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

class Main {
    public static void main(String[] args) throws IOException {
        new AnnotationConfigApplicationContext(SpringConfig.class)
                .getBean(Cart.class)
                .startInteraction();
    }
}
