package org.spring;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

public class Cart {
    private final Map<Long, Product> productsById = new HashMap<>();

    static ProductRepository repository;

    static {
        repository = new AnnotationConfigApplicationContext(SpringConfig.class)
                .getBean(ProductRepository.class);
    }

    public void addProduct(long productId) {
        var product = repository.getProductById(productId);
        if (product == null) {
            throw new IllegalArgumentException("Product with id=" + productId + " can't be added");
        }
        productsById.put(productId, product);
    }

    public void removeProduct(long productId) {
        productsById.remove(productId);
    }

    public void startInteraction() throws IOException {
        final var inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));
        final var outToConsole = new PrintWriter(System.out, true);
        while (true) {
            if (inFromKeyboard.ready()) {
                String command = inFromKeyboard.readLine().trim();
                if (command.matches("add \\d+")) {
                    try {
                        long id = Long.parseLong(command.substring(4));
                        addProduct(id);
                    } catch (IllegalArgumentException ex) {
                        outToConsole.println(ex.getMessage());
                    }
                } else if (command.matches("rmv \\d+")) {
                    long id = Long.parseLong(command.substring(4));
                    removeProduct(id);
                } else {
                    outToConsole.println("Unknown command. Available commands: add [id], rmv [id]");
                }
            }
        }
    }
}

