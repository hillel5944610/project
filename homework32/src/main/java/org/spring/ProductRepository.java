package org.spring;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Objects;

public class ProductRepository {

    @Getter @Setter(onParam_={@NonNull})
    private List<Product> products;

    @Autowired
    public ProductRepository(@NonNull List<Product> products) {
        this.products = products;
    }

    public void printProducts() {
        products.forEach(System.out::println);
    }

    public Product getProductById(Long id) {
        for (var product : products) {
            if (Objects.equals(product.getId(), id)) {
                return product;
            }
        } return null;
    }
}
