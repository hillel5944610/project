package org.spring;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class SpringConfig {
    @Bean
    public ProductRepository productRepository() {
        return new ProductRepository(products());
    }

    @Bean
    public List<Product> products() {
        return new ArrayList<>(List.of(productA(), productB(), productC()));
    }

    @Bean
    public Product productA() {
        return new Product("A", 1);
    }

    @Bean
    public Product productB() {
        return new Product("B", 2);
    }

    @Bean
    public Product productC() {
        return new Product("C", 3);
    }

    @Bean
    @Scope("prototype")
    public Cart cart() {
        return new Cart();
    }
}
