package com.homeworks.homework35.controllers;

import com.homeworks.homework35.Homework35Application;
import com.homeworks.homework35.persist.OrderRepository;
import com.homeworks.homework35.persist.ProductRepository;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.homeworks.homework35.model.Order;
import com.homeworks.homework35.model.Product;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class OrderControllerTest {
    @Autowired
    TestRestTemplate restTemplate;

    OrderRepository orderRepository;
    ProductRepository productRepository;

    OrderControllerTest() {
        ApplicationContext context = new AnnotationConfigApplicationContext(Homework35Application.class);
        orderRepository = context.getBean(OrderRepository.class);
        productRepository = context.getBean(ProductRepository.class);
    }

    @DirtiesContext
    @BeforeEach
    void setUp() {
        var productA = new Product("A", 1);
        var productB = new Product("B", 2);
        var productC = new Product("C", 3);
        var productD = new Product("D", 4);
        productRepository.saveAllAndFlush(List.of(productA, productB, productC, productD));
        var productQuantityMapAB = Map.of(productA, 1, productB, 2);
        var productQuantityMapCD = Map.of(productC, 3, productD, 4);
        var order1 = new Order(productQuantityMapAB);
        var order2 = new Order(productQuantityMapCD);
        orderRepository.saveAllAndFlush(List.of(order1, order2));
    }

    @Test
    void shouldReturnAllOrders() {
        ResponseEntity<String> response = restTemplate.getForEntity("/orders", String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext = JsonPath.parse(response.getBody());
        int orderCount = documentContext.read("$.length()");
        assertThat(orderCount).isEqualTo(2);

        JSONArray orderIds = documentContext.read("$..id");
        assertThat(orderIds).containsExactlyInAnyOrder(1, 2);
    }

    @Test
    void shouldReturnAvailableOrder() {
        ResponseEntity<String> response1 = restTemplate.getForEntity("/orders/1", String.class);
        ResponseEntity<String> response2 = restTemplate.getForEntity("/orders/2", String.class);

        assertThat(response1.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.OK);

        DocumentContext documentContext1 = JsonPath.parse(response1.getBody());
        DocumentContext documentContext2 = JsonPath.parse(response2.getBody());

        Number orderId1 = documentContext1.read("$.id");
        Number orderId2 = documentContext2.read("$.id");

        assertThat(orderId1).isEqualTo(1);
        assertThat(orderId2).isEqualTo(2);

        Double orderCost1 = documentContext1.read("$.cost");
        Double orderCost2 = documentContext2.read("$.cost");

        assertThat(orderCost1).isCloseTo(5.0, offset(0.001));
        assertThat(orderCost2).isCloseTo(25.0, offset(0.001));

        JSONArray productIdsFromOrder1 = documentContext1.read("$.productQuantityMap..id");
        JSONArray productIdsFromOrder2 = documentContext2.read("$.productQuantityMap..id");

        assertThat(productIdsFromOrder1).containsExactlyInAnyOrder(1, 2);
        assertThat(productIdsFromOrder2).containsExactlyInAnyOrder(3, 4);
    }

    @Test
    void shouldNotReturnUnavailableOrder() {
        ResponseEntity<String> response = restTemplate.getForEntity("/orders/1000", String.class);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(response.getBody()).isBlank();
    }
}
