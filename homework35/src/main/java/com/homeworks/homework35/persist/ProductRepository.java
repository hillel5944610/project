package com.homeworks.homework35.persist;

import com.homeworks.homework35.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    boolean existsById(long id);
}
