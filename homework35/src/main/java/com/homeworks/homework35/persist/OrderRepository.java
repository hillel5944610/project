package com.homeworks.homework35.persist;

import com.homeworks.homework35.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, Long> {
    boolean existsById(long id);
}
