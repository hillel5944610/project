package com.homeworks.homework35.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.time.Instant;
import java.util.Map;

@Entity
@Table(name = "orders")
@Getter @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Order {
    @Id
    @GeneratedValue
    private Long id;

    private Instant timeCreated;

    @ElementCollection
    @CollectionTable(name = "order_products")
    @MapKeyJoinColumn(name = "product_id")
    @Column(name = "product_quantity")
    private Map<@NotNull Product, @NotNull Integer> productQuantityMap;

    private double cost;

    public Order(@NonNull Map<Product, Integer> productQuantityMap) {
        if (productQuantityMap.isEmpty()) throw new IllegalArgumentException("Order can't be empty");
        this.timeCreated = Instant.now();
        this.productQuantityMap = productQuantityMap;
        cost = productQuantityMap.keySet()
                .stream()
                .mapToDouble(product -> product.getPrice() * productQuantityMap.get(product))
                .sum();
    }
}
