package com.homeworks.homework35.controllers;

import com.homeworks.homework35.model.Order;
import com.homeworks.homework35.persist.OrderRepository;
import com.homeworks.homework35.persist.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
@Slf4j
public class OrderController {
    private final OrderRepository orderRepository;
    private final ProductRepository productRepository;

    @Autowired
    public OrderController(OrderRepository orderRepository, ProductRepository productRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
    }

    @GetMapping("/{orderId}")
    public ResponseEntity<Order> getOrderById(@PathVariable long orderId) {
        log.trace("GET request for order with id " + orderId);
        Optional<Order> order = orderRepository.findById(orderId);
        return order.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<Iterable<Order>> getAllOrders() {
        log.trace("GET request for all orders");
        return ResponseEntity.ok(orderRepository.findAll());
    }

    @PostMapping
    public ResponseEntity<Void> createOrder(@RequestBody Order orderToCreate, UriComponentsBuilder ucb) {
        log.trace("POST request for " + orderToCreate);
        boolean allProductsPersistent = orderToCreate.getProductQuantityMap()
                .keySet().stream()
                .allMatch(product -> productRepository.existsById(product.getId()));
        if (!allProductsPersistent) {
            return ResponseEntity.badRequest().build();
        }
        // create new order with null id to avoid update of existing order saved in database
        Order newOrder = new Order(orderToCreate.getProductQuantityMap());
        Order savedOrder = orderRepository.save(newOrder);
        URI orderLocation = ucb
                .path("orders/{id}")
                .buildAndExpand(savedOrder.getId())
                .toUri();
        return ResponseEntity.created(orderLocation).build();
    }

    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteCashCard(@PathVariable Long id) {
        log.trace("DELETE request for order with id=" + id);
        if (!orderRepository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        orderRepository.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
