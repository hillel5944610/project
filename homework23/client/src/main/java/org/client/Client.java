package org.client;

import org.common.requests.*;
import org.common.nio_utils.SocketChannelUtil;
import org.common.responses.*;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Set;

public class Client {
    private static final int PORT_NUMBER = 8021;

    private static Path filePath;

    public static void main(String[] args) throws IOException {
        var socketChannel = SocketChannel.open(new InetSocketAddress("localhost", PORT_NUMBER));
        socketChannel.configureBlocking(false);
        var selector = Selector.open();
        socketChannel.register(selector, SelectionKey.OP_READ);
        var inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));
        var outToConsole = new PrintWriter(System.out, true);
        while (true) {
            if (selector.selectNow() > 0) {
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                outToConsole.println(selectedKeys);
                var iterator = selectedKeys.iterator();
                while (iterator.hasNext()) {
                    var key = iterator.next();
                    if (key.isReadable()) {
                        var serverResponse = (ServerResponse) SocketChannelUtil.readObjectFrom((SocketChannel) key.channel());
                        if (serverResponse instanceof CatalogsResponse catalogsResponse) {
                            outToConsole.println(catalogsResponse.getCatalogs());
                        } else if (serverResponse instanceof FilesResponse filesResponse)  {
                            outToConsole.println(filesResponse.getFileNames());
                        } else if (serverResponse instanceof PutResponse putResponse && putResponse.isAccepted()) {
                            sendFileThroughChannel(filePath, socketChannel);
                        } else if (serverResponse instanceof FindResponse findResponse) {
                            outToConsole.println(findResponse.getCatalogId());
                        }
                    }
                    iterator.remove();
                }
            }
            String command;
            if (inFromKeyboard.ready() && (command = inFromKeyboard.readLine()) != null) {
                command = command.strip();
                if (command.equals("catalogs")) {
                    SocketChannelUtil.sendObjectTo(new CatalogsRequest(), socketChannel);
                } else if (command.matches("use .+")) {
                    String catalogName = command.substring(4);
                    SocketChannelUtil.sendObjectTo(new UseRequest(catalogName), socketChannel);
                } else if (command.equals("files")) {
                    SocketChannelUtil.sendObjectTo(new FilesRequest(), socketChannel);
                } else if (command.matches("files .+")) {
                    String catalogName = command.substring(6);
                    SocketChannelUtil.sendObjectTo(new FilesRequest(catalogName), socketChannel);
                } else if (command.matches("put .+")) {
                    String fileName = command.substring(4);
                    try {
                        filePath = Path.of(fileName);
                        System.out.println(Files.isRegularFile(filePath));
                        System.out.println(Files.isReadable(filePath));
                        if (Files.exists(filePath) && Files.isRegularFile(filePath) && Files.isReadable(filePath)) {
                            SocketChannelUtil.sendObjectTo(new PutRequest(filePath.getFileName().toString(), Files.size(filePath)), socketChannel);
                        } else {
                            outToConsole.println("File can't be sent");
                        }
                    } catch (InvalidPathException ex) {
                        outToConsole.println("Invalid path");
                    }
                } else if (command.matches("find .+")) {
                    String fileName = command.substring(5);
                    SocketChannelUtil.sendObjectTo(new FindRequest(fileName), socketChannel);
                } else if (command.equals("bye")) {
                    SocketChannelUtil.sendObjectTo(new ByeRequest(), socketChannel);
                    socketChannel.close();
                    break;
                }
            }
        }
    }

    private static void sendFileThroughChannel(Path file, SocketChannel socketChannel) throws IOException {
        long remaining = Files.size(file);
        var buffer = ByteBuffer.allocate(2048);
        var fileChannel = FileChannel.open(file, StandardOpenOption.READ);
        do {
            int readBytes = fileChannel.read(buffer);
            remaining -= readBytes;
            buffer.flip();
            socketChannel.write(buffer);
            buffer.compact();
        } while (remaining > 0 || buffer.hasRemaining());
    }
}
