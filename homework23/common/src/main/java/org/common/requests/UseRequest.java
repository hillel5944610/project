package org.common.requests;

public class UseRequest implements ClientRequest {
    private String catalogName;

    public UseRequest(String catalogName) {
        this.catalogName = catalogName;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }
}
