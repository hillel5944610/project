package org.common.requests;

public class FilesRequest implements ClientRequest {
    private String catalogId;

    public FilesRequest() { }

    public FilesRequest(String catalogId) {
        this.catalogId = catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogId() {
        return catalogId;
    }
}
