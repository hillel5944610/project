package org.common.requests;

public class FindRequest implements ClientRequest {
    private String fileName;

    public FindRequest(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
