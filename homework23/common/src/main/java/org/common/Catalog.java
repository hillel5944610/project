package org.common;

import java.io.Serializable;
import java.nio.file.Path;

public record Catalog(String id, Path dir) implements Serializable {

}
