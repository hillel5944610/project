package org.common.responses;

public class PutResponse implements ServerResponse {

    private boolean accepted;

    public PutResponse(boolean accepted) {
        this.accepted = accepted;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

}
