package org.common.responses;

import org.common.Catalog;

import java.util.Collection;

public class CatalogsResponse implements ServerResponse {
    private final Collection<Catalog> catalogs;
    private final Catalog chosenCatalog;

    public CatalogsResponse(Collection<Catalog> catalogs, Catalog chosenCatalog) {
        this.catalogs = catalogs;
        this.chosenCatalog = chosenCatalog;
    }

    public Catalog getChosenCatalog() {
        return chosenCatalog;
    }

    public Collection<Catalog> getCatalogs() {
        return catalogs;
    }
}
