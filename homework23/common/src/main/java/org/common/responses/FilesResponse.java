package org.common.responses;

import java.util.Collection;

public class FilesResponse implements ServerResponse {
    private Collection<String> fileNames;

    public FilesResponse(Collection<String> fileNames) {
        this.fileNames = fileNames;
    }

    public Collection<String> getFileNames() {
        return fileNames;
    }

    public int return1() {
        return  1;
    }

    public void setFileNames(Collection<String> fileNames) {
        this.fileNames = fileNames;
    }
}
