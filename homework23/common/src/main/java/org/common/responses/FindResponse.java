package org.common.responses;

public class FindResponse implements ServerResponse {

    private String catalogId;

    public FindResponse(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

}
