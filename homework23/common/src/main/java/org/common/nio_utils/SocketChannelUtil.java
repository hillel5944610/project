package org.common.nio_utils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class SocketChannelUtil {
    public static void sendObjectTo(Serializable object, SocketChannel target) throws IOException {
        var memory = new ByteArrayOutputStream();
        var objOut = new ObjectOutputStream(memory);
        objOut.writeObject(object);
        memory.flush();
        var buffer = ByteBuffer.wrap(memory.toByteArray());
        while (buffer.hasRemaining()) {
            target.write(buffer);
        }
    }

    public static Object readObjectFrom(SocketChannel source) throws IOException {
        var buffer = ByteBuffer.allocate(512);  // hardcoded value, subject to change
        source.read(buffer);
        buffer.flip();
        Object obj;
        try {
            obj = new ObjectInputStream(new ByteArrayInputStream(buffer.array())).readObject();
        } catch (ClassNotFoundException ex) {
            return null;
        }

        return obj;
    }
}
