package org.server.config;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.common.Catalog;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class ServerConfigTest {
    private Path propertiesFile;

    @Test
    void shouldSetValuesParsedFromFileWithCorrectContent() throws IOException {
        propertiesFile = Path.of("src/test/resources/var1.properties");
        String fileContent = """
                               server.port=6543
                             file.timeout = 3
                               catalog.var1.dir    =    files
                             """;
        Files.writeString(propertiesFile, fileContent, StandardCharsets.UTF_8);

        ServerConfig config = ServerConfig.fromFile(propertiesFile);

        assertEquals(6543, config.getPortNumber());
        assertEquals(3, config.getTimeOut());
        assertIterableEquals(
                Set.of(new Catalog("var1", Path.of("files"))),
                config.getCatalogs()
        );
    }

    @Test
    void shouldSetDefaultValuesIfFileContentIsNotCorrect() throws IOException {
        propertiesFile = Path.of("src/test/resources/var1.properties");
        String fileContent = """
                             #server.port = 6543
                             server,port = 6543
                             = server.port
                             server.port = hello
                                        server.port =
                                        server.port = -1
                                        file.timeout = -2
                                        catalog.var2.dir = wrong/path
                             """;
        Files.writeString(propertiesFile, fileContent, StandardCharsets.UTF_8);

        ServerConfig config = ServerConfig.fromFile(propertiesFile);

        assertEquals(ServerConfig.DEFAULT_PORT_NUMBER, config.getPortNumber());
        assertEquals(ServerConfig.DEFAULT_TIMEOUT, config.getTimeOut());
        assertIterableEquals(
                Collections.emptySet(),
                config.getCatalogs()
        );
    }

    @Test
    void shouldNotAcceptWrongFormatFiles() {
        propertiesFile = Path.of("src/test/resources/var1.FORMAT");

        assertThrows(
                IllegalArgumentException.class,
                () -> ServerConfig.fromFile(propertiesFile)
        );
    }

    @AfterEach
    void cleanUp() throws IOException {
        if (Files.exists(propertiesFile)) {
            Files.delete(propertiesFile);
        }
    }
}