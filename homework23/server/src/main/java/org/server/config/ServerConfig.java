package org.server.config;

import lombok.NonNull;
import org.common.Catalog;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

public interface ServerConfig {
    int DEFAULT_PORT_NUMBER = 8021;
    int DEFAULT_TIMEOUT = 2;

    int getPortNumber();

    long getTimeOut();

    Collection<Catalog> getCatalogs();

    static ServerConfig fromFile(@NonNull Path propertiesFile) throws IOException {
        return fromFile(propertiesFile, StandardCharsets.UTF_8);
    }

    static ServerConfig fromFile(@NonNull Path propertiesFile, @NonNull Charset charset) throws IOException {
        if (!propertiesFile.toString().matches(".*\\.properties")) {
            throw new IllegalArgumentException("Wrong file extension");
        }

        int portNumber = DEFAULT_PORT_NUMBER;
        int timeOut = DEFAULT_TIMEOUT;
        Collection<Catalog> catalogs = new HashSet<>();

        List<String> lines = Files.readAllLines(propertiesFile, charset);

        for (String line : lines) {
            line = line.strip();
            int equalSignPos = line.indexOf('=');
            if (line.startsWith("#")  // comment line
                    || equalSignPos <= 0  // no '=' sign or starts with it
                    || equalSignPos == line.length() - 1) {  // ends with '='
                continue;
            }
            String key = line.substring(0, equalSignPos).strip();  // before '='
            String value = line.substring(equalSignPos + 1).strip();  // after '='
            if (key.equals("server.port")) {
                try {
                    int number = Integer.parseInt(value);
                    if (number > 1024 && number < 65535)
                        portNumber = number;
                } catch (NumberFormatException ignored) { }
            } else if (key.equals("file.timeout")) {
                try {
                    int number = Integer.parseInt(value);
                    if (number > 0)
                        timeOut = number;
                } catch (NumberFormatException ignored) { }
            } else if (key.matches("catalog\\.\\w+\\.dir")) {
                Path path = null;
                try {
                    path = Path.of(value);
                } catch (InvalidPathException ignored) { }
                if (path == null || !Files.isDirectory(path)) {
                    continue;
                }
                String catalogId = key.substring(8, key.lastIndexOf(".dir"));
                catalogs.add(new Catalog(catalogId, path));
            }
        }
        return new ServerConfigImpl(portNumber, timeOut, catalogs);
    }
}
