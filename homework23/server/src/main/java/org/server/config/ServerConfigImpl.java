package org.server.config;

import lombok.NonNull;
import org.common.Catalog;

import java.util.Collection;

public class ServerConfigImpl implements ServerConfig {

    private final int portNumber;
    private final int timeOut;
    private final Collection<Catalog> catalogs;

    ServerConfigImpl(int portNumber, int timeOut, @NonNull Collection<Catalog> catalogs) {
        this.portNumber = portNumber;
        this.timeOut = timeOut;
        this.catalogs = catalogs;
    }

    @Override
    public int getPortNumber() {
        return portNumber;
    }

    @Override
    public long getTimeOut() {
        return timeOut;
    }

    @Override
    public Collection<Catalog> getCatalogs() {
        return catalogs;
    }
}
