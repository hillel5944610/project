package org.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.common.Catalog;
import org.common.nio_utils.SocketChannelUtil;
import org.common.requests.*;
import org.common.responses.CatalogsResponse;
import org.common.responses.FindResponse;
import org.common.responses.PutResponse;
import org.server.config.ServerConfig;
import org.server.model.ClientInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.stream.Collectors;

public class Server {
    private static final Logger logger = LogManager.getLogger();

    private final RequestProcessor requestProcessor = new RequestProcessor();
    private final ServerConfig serverConfig;

    public Server(ServerConfig serverConfig) {
        this.serverConfig = Objects.requireNonNull(serverConfig);
    }

    public void start() throws IOException {
        var serverSocketChannel = ServerSocketChannel.open().bind(new InetSocketAddress(serverConfig.getPortNumber()));
        serverSocketChannel.configureBlocking(false);
        Selector selector = Selector.open();
        var inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));
        var outToConsole = new PrintWriter(System.out, true);

        logger.info("Server started");

        while (true) {
            SocketChannel socketChannel;
            if (serverSocketChannel.isOpen() && (socketChannel = serverSocketChannel.accept()) != null) {
                socketChannel.configureBlocking(false);
                socketChannel.register(
                        selector, SelectionKey.OP_READ, new ClientInfo());
                logger.info("New connection from " + socketChannel.getRemoteAddress());
            }
            if (selector.select(serverConfig.getTimeOut()) > 0) {
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                var iterator = selectedKeys.iterator();
                while (iterator.hasNext()) {
                    var key = iterator.next();
                    if (key.isReadable()) {
                        var clientRequest = (ClientRequest) SocketChannelUtil.readObjectFrom((SocketChannel) key.channel());
                        var clientInfo = (ClientInfo) key.attachment();

                        logger.trace(clientRequest + " from " + ((SocketChannel) key.channel()).getRemoteAddress());

                        clientInfo.addRequest(clientRequest);
                        requestProcessor.processRequest(clientRequest, key);
                    }
                    iterator.remove();
                }
            }
            String command;
            if (inFromKeyboard.ready() && (command = inFromKeyboard.readLine()) != null) {
                command = command.strip();
                switch (command) {
                    case "start" -> {
                        if (serverSocketChannel.isOpen()) continue;
                        serverSocketChannel = ServerSocketChannel.open().bind(new InetSocketAddress(serverConfig.getPortNumber()));
                        serverSocketChannel.configureBlocking(false);
                        logger.info("Server resumed");
                    }
                    case "stop" -> {
                        serverSocketChannel.close();
                        logger.info("Server stopped");
                    }
                    case "info" -> {
                        outToConsole.println("Local port: " + serverConfig.getPortNumber());
                        var keySet = selector.keys();
                        for (var key : keySet) {
                            outToConsole.println("Remote address: " + ((SocketChannel) key.channel()).getRemoteAddress());
                            var clientInfo = (ClientInfo) key.attachment();
                            outToConsole.println("Last requests: " + clientInfo.getRequests());
                        }
                    }
                    case "exit" -> {
                        logger.info("Finished execution");
                        System.exit(0);
                    }
                    default -> outToConsole.println("Unknown command. Available requests: start, stop, info, exit");
                }
            }
        }
    }

    class RequestProcessor {
        private SelectionKey key;
        private ClientInfo clientInfo;

        public void processRequest(ClientRequest clientRequest, SelectionKey key) throws IOException {
            this.key = key;
            this.clientInfo = (ClientInfo) key.attachment();
            if (clientRequest instanceof CatalogsRequest) {
                processCatalogsRequest();
            } else if (clientRequest instanceof UseRequest useRequest) {
                processUseRequest(useRequest);
            } else if (clientRequest instanceof FilesRequest filesRequest) {
                processFilesRequest(filesRequest);
            } else if (clientRequest instanceof PutRequest putRequest) {
                processPutRequest(putRequest);
            } else if (clientRequest instanceof FindRequest findRequest) {
                processFindRequest(findRequest);
            } else if (clientRequest instanceof ByeRequest) {
                processByeRequest();
            }
        }

        private void processCatalogsRequest() throws IOException {
            Catalog chosenCatalog = clientInfo.getChosenCatalog();
            SocketChannelUtil.sendObjectTo(
                    new CatalogsResponse(serverConfig.getCatalogs(), chosenCatalog), (SocketChannel) key.channel());
        }

        private void processUseRequest(UseRequest useRequest) {
            serverConfig.getCatalogs().stream()
                    .filter(catalog -> catalog.id().equals(useRequest.getCatalogName()))
                    .findAny()
                    .ifPresent(clientInfo::setChosenCatalog);
        }

        private void processFilesRequest(FilesRequest filesRequest) throws IOException {
            String catalogId = filesRequest.getCatalogId();

            Path catalogPath;

            if (catalogId != null) {
                Optional<Catalog> catalog = serverConfig.getCatalogs().stream()
                        .filter(c -> c.id().equals(catalogId))
                        .findAny();
                catalogPath = catalog.map(Catalog::dir).orElse(null);
            } else {
                var catalog = clientInfo.getChosenCatalog();
                catalogPath = catalog == null ? null : catalog.dir();
            }

            if (catalogPath == null) {
                SocketChannelUtil.sendObjectTo(null, (SocketChannel) key.channel());
                return;
            }

            SocketChannelUtil.sendObjectTo(
                    Files.list(catalogPath)
                            .map(file -> file.getFileName().toString())
                            .collect(Collectors.toCollection(ArrayList::new)),
                    (SocketChannel) key.channel()
            );
        }

        private void processPutRequest(PutRequest putRequest) throws IOException {
            PutResponse response = new PutResponse(true);
            if (clientInfo.getChosenCatalog() == null
            || Files.exists(clientInfo.getChosenCatalog().dir().resolve(putRequest.getFileName()))) {
                response.setAccepted(false);
            }
            SocketChannel socketChannel = (SocketChannel) key.channel();
            SocketChannelUtil.sendObjectTo(response, socketChannel);
            if (!response.isAccepted()) {
                return;
            }
            getFileFromChannel(putRequest, socketChannel);
        }

        private void getFileFromChannel(PutRequest putRequest, SocketChannel socketChannel) throws IOException {
            long remaining = putRequest.getFileSize();
            var buffer = ByteBuffer.allocate(2048);
            Path targetPath = clientInfo.getChosenCatalog().dir().resolve(putRequest.getFileName());
            var fileChannel = FileChannel.open(targetPath,
                    StandardOpenOption.CREATE, StandardOpenOption.WRITE);
            do {
                int readBytes = socketChannel.read(buffer);
                remaining -= readBytes;
                buffer.flip();
                fileChannel.write(buffer);
                buffer.compact();
            } while (remaining > 0 || buffer.hasRemaining());

            logger.info("Received " + targetPath + " from " + ((SocketChannel) key.channel()).getRemoteAddress());
        }

        private void processFindRequest(FindRequest findRequest) throws IOException {
            String fileName = findRequest.getFileName();
            String catalogId = null;
            for (Catalog catalog : serverConfig.getCatalogs()) {
                Path path = catalog.dir();
                if (Files.exists(path.resolve(fileName))) {
                    catalogId = catalog.id();
                    break;
                }
            }
            SocketChannelUtil.sendObjectTo(
                    new FindResponse(catalogId  == null ? fileName + " was not found" : catalogId),
                    (SocketChannel) key.channel()
            );
        }

        private void processByeRequest() throws IOException {
            key.channel().close();
            key.cancel();

            logger.info(((SocketChannel) key.channel()).getRemoteAddress() + " disconnected");

            key = null;
            clientInfo = null;
        }
    }

}
