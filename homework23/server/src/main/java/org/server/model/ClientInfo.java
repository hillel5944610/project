package org.server.model;

import org.common.Catalog;
import org.common.requests.ClientRequest;

import java.util.LinkedList;
import java.util.List;

public class ClientInfo {
    private final List<ClientRequest> requests = new LinkedList<>();
    private Catalog chosenCatalog;

    public void addRequest(ClientRequest clientRequest) {
        if (requests.size() == 3) {
            requests.remove(0);
        }
        requests.add(clientRequest);
    }

    public List<ClientRequest> getRequests() {
        return requests;
    }

    public Catalog getChosenCatalog() {
        return chosenCatalog;
    }

    public void setChosenCatalog(Catalog chosenCatalog) {
        this.chosenCatalog = chosenCatalog;
    }
}
