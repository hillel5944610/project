
create table homework25.schedule (
    id serial primary key,
    "name" varchar(20) not null,
    updatedAt timestamp
);

create table homework25.lessons (
    id serial primary key,
    "name" varchar(20) not null,
    updatedAt timestamp
);

create table homework25.lessons_by_schedule (
    schedule_id integer not null,
    lesson_id integer not null,
    primary key (schedule_id, lesson_id),
    constraint fk_schedule
      foreign key(schedule_id)
        references homework25.schedule(id),
    constraint fk_lesson
      foreign key(lesson_id)
        references homework25.lessons(id)
);

create table homework25.homeworks (
	lesson_id integer primary key,
	"name" varchar(20) not null,
	description varchar(100),
	constraint fk_lesson
      foreign key(lesson_id)
        references homework25.lessons(id)
);
