package org.homeworks.homework10;

import java.util.ArrayList;
import java.util.Arrays;

class ArrayToList {

    public static <E> ArrayList<E> toList(E[] array) {
        if (array == null) return null;
        return new ArrayList<>(Arrays.asList(array));
    }

}

public class Box <E extends Fruit> {

    private final ArrayList<E> content = new ArrayList<>();

    public boolean add(E fruit) {
        if (fruit != null) {
            content.add(fruit);
            return true;
        }
        return false;
    }

    @SafeVarargs
    public final boolean addAll(E... fruits) {
        for (E fruit : fruits) {
            if (!add(fruit)) {
                return false;
            }
        }
        return true;
    }

    public double getWeight() {
        if (content.isEmpty()) return 0;
        return content.stream()
                .mapToDouble(Fruit::getWeight)
                .sum();
    }

    public boolean compare(Box<? extends Fruit> anotherBox) {
        return this.getWeight() == anotherBox.getWeight();
    }

    public void mergeTo(Box<? super E> anotherBox) {
        for (int i = content.size() - 1; i >= 0; i--) {
            if (!anotherBox.add(content.get(i))) {
                throw new NullPointerException();
            }
            content.remove(i);
        }
    }

}

abstract class Fruit {

    private double weight;

    protected Fruit(double weight) {
        this.weight = weight;
    }

    public double getWeight() {
        return weight;
    }

}

class Apple extends Fruit {

    private static final double DEFAULT_APPLE_WEIGHT = 1.0;

    public Apple() {
        super(DEFAULT_APPLE_WEIGHT);
    }

}

class Orange extends Fruit {

    private static final double DEFAULT_ORANGE_WEIGHT = 1.5;

    public Orange() {
        super(DEFAULT_ORANGE_WEIGHT);
    }

}
