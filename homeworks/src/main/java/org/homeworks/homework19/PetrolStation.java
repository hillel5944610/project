package org.homeworks.homework19;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Semaphore;


class ThreadSafeList<E> implements Iterable<E> {

    private final List<E> list = new CopyOnWriteArrayList<>();

    public boolean add(E e) {
        return list.add(e);
    }

    public void add(int index, E e) {
        list.add(index, e);
    }

    public boolean remove(E e) {
        return list.remove(e);
    }

    public E remove(int index) {
        return list.remove(index);
    }

    public E get(int index) {
        return list.get(index);
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

}

public class PetrolStation {
    private static final int MAX_CAPACITY = 3;

    private double availableAmount;
    private final Semaphore semaphore = new Semaphore(MAX_CAPACITY, true);

    public PetrolStation(double amountOfFuel) {
        if (amountOfFuel < 0) throw new IllegalArgumentException();
        this.availableAmount = amountOfFuel;
    }

    public void doRefuel(double amountOfFuel) {
        try {
            semaphore.acquire();
            if (amountOfFuel < 0) throw new IllegalArgumentException();

            Thread.sleep((long) (3000 + Math.random() * 7000));

            synchronized (this) {
                if (availableAmount < amountOfFuel) amountOfFuel = availableAmount;
                availableAmount -= amountOfFuel;
            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        } finally {
            semaphore.release();
        }
    }

    public double getAvailableAmount() {
        return availableAmount;
    }

}
