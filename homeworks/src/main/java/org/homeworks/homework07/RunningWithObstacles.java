package org.homeworks.homework07;

public class RunningWithObstacles {
    public static void main(String[] args) {
        Obstacle[] obstacles = new Obstacle[2];
        obstacles[0] = new Wall(1.5);
        obstacles[1] = new RaceTrack(50000.0);
        Member[] members = {new Human("Gosha", 2, 40000),
                new Cat("Kisa", 1,10000),
                new Robot("Terminator", 3, 100000)};
        for (Member member : members)
            for (Obstacle obstacle : obstacles) {
                if (member.overcome(obstacle)) {
                    System.out.println(member + " overcame " + obstacle);
                } else {
                    System.out.println(member + " did not overcome " + obstacle);
                    break;
                }
            }
        System.out.println(members[0].overcome(new Wall(2.0)));
    }
}

abstract class Member {
    private String name;
    private double maxJump;
    private double maxRun;
    protected Member(String name, double maxJump, double maxRun) {
        this.name = name;
        this.maxJump = maxJump;
        this.maxRun = maxRun;
    }
    public String getName() {
        return name;
    }
    public void jump() {
        System.out.println(this + " jumped");
    }
    public void run() {
        System.out.println(this + " started running");
    }
    public boolean overcome(Obstacle obstacle) {
        if (obstacle instanceof Wall)
            return overcome((Wall) obstacle);
        else
            return overcome((RaceTrack) obstacle);
    }
    public boolean overcome(Wall wall) {
        this.jump();
        return getMaxJump() >= wall.getHeight();
    }
    public boolean overcome(RaceTrack raceTrack) {
        this.run();
        return getMaxRun() >= raceTrack.getLength();
    }
    public double getMaxJump() {
        return maxJump;
    }
    public double getMaxRun() {
        return maxRun;
    }
}

class Human extends Member {
    public Human(String name, double maxJump, double maxRun) {
        super(name, maxJump, maxRun);
    }
    @Override
    public String toString() {
        return "Human " + getName();
    }
}

class Cat extends Member {
    public Cat(String name, double maxJump, double maxRun) {
        super(name, maxJump, maxRun);
    }
    @Override
    public String toString() {
        return "Cat " + getName();
    }
}

class  Robot extends Member {
    public Robot(String name, double maxJump, double maxRun) {
        super(name, maxJump, maxRun);
    }
    @Override
    public String toString() {
        return "Robot " + getName();
    }
}

abstract class Obstacle {

}

class Wall extends Obstacle {
    private double height;
    public Wall(double height) {
        this.height = height;
    }
    @Override
    public String toString() {
        return getHeight() + " meters height wall";
    }
    public double getHeight() {
        return height;
    }
}

class RaceTrack extends Obstacle {
    private double length;
    public RaceTrack(double length) {
        this.length = length;
    }
    @Override
    public String toString() {
        return getLength() + " meters length racetrack";
    }
    public double getLength() {
        return length;
    }
}
