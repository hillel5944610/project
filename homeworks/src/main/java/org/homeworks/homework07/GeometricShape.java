package org.homeworks.homework07;

class Main {
    public static double totalArea(GeometricShape[] arr) {
        double sum = 0;
        for (GeometricShape element : arr)
            sum += element.getArea();
        return sum;
    }
    public static void main(String[] args) {
        GeometricShape[] array = new GeometricShape[3];
        array[0] = new Circle(1);
        array[1] = new Triangle(3, 4, 5);
        array[2] = new Square(10);
        System.out.println(totalArea(array));
    }

}

public interface GeometricShape {
    double getPerimeter();

    double getArea();
}

class Circle implements GeometricShape {
    private double radius;
    public Circle(double radius) {
        if (radius < 0)
            throw new IllegalArgumentException("radius can`t be negative");
        else
            this.radius = radius;
    }
    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }
    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
}

class Triangle implements GeometricShape {
    private double[] sides = new double[3];
    public Triangle(double side1, double side2, double side3) {
        if (side1 < 0 || side2 < 0 || side3 < 0)
            throw new IllegalArgumentException("Length of side can`t be negative");
        else if (side1 > (side2 + side3) ||
                side2 > (side1 + side3) ||
                side3 > (side1 + side2))
            throw new IllegalArgumentException("Sum of lengths of any two sides should be " +
                    "greater than the length of the third side");
        else {
            sides[0] = side1;
            sides[1] = side2;
            sides[2] = side3;
        }
    }
    @Override
    public double getPerimeter() {
        double perimeter = 0;
        for (double side : sides)
            perimeter += side;
        return perimeter;
    }
    @Override
    public double getArea() {
        double halfPerimeter = getPerimeter() / 2;
        return Math.sqrt(halfPerimeter * (halfPerimeter - sides[0]) * // Heron’s formula
                (halfPerimeter - sides[1]) *
                (halfPerimeter - sides[2]));
    }
}

class Square implements GeometricShape {
    private double side;
    public Square(double side) {
        if (side < 0)
            throw new IllegalArgumentException("Length of side can`t be negative");
        else
            this.side = side;
    }
    @Override
    public double getArea() {
        return side * side;
    }
    @Override
    public double getPerimeter() {
        return side * 4;
    }
}
