package org.homeworks.homework12;


import java.util.*;
import java.util.stream.Collectors;


class Main {

    public static long foundOccurrence(List<String> list, String target) {
        Objects.requireNonNull(list);
        return  list.stream()
                .filter(e -> Objects.equals(e, target))
                .count();
    }

    public static <T> List<T> toList(T[] arr) {
        return List.of(arr);
    }

    public static <T> List<T> findUnique(List<T> source) {
        Objects.requireNonNull(source);
        var set = new LinkedHashSet<>(source);
        return new ArrayList<>(set);
    }

    public static Map<String, Long> calcOccurrence(List<String> list) {
        Objects.requireNonNull(list);
        return list.stream()
                    .filter(Objects::nonNull)
                    .collect(Collectors.groupingBy(e -> e, HashMap::new, Collectors.counting()));
    }

}

public class Phonebook {

    private final HashMap<String, HashSet<Entry>> entriesByName = new HashMap<>();

    public boolean add(Entry e) {
        Objects.requireNonNull(e);
        if (entriesByName.containsKey(e.name())) {
            return entriesByName.get(e.name()).add(e);
        }
        HashSet<Entry> entries = new HashSet<>();
        entries.add(e);
        entriesByName.put(e.name(), entries);
        return true;
    }

    public Entry find(String name) {
        if (entriesByName.containsKey(name)) {
            return (Entry) entriesByName.get(name).toArray()[0];
        }
        return null;
    }

    public HashSet<Entry> findAll(String name) {
        return entriesByName.get(name);
    }

    public static class Entry {

        private final String name;
        private final String phoneNumber;

        public Entry(String name, String phoneNumber) {
            Objects.requireNonNull(name);
            Objects.requireNonNull(phoneNumber);
            if (name.isBlank() || !phoneNumber.matches("\\+380[\\d]{9}")) {
                throw new IllegalArgumentException();
            }
            this.name = name;
            this.phoneNumber = phoneNumber;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) return false;
            if (obj == this) return true;
            return obj instanceof Entry entry
                    && name.equals(entry.name())
                    && phoneNumber.equals(entry.phoneNumber());
        }

        private String phoneNumber() {
            return phoneNumber;
        }

        private String name() {
            return name;
        }

        @Override
        public int hashCode() {
            return Objects.hash(name, phoneNumber);
        }

    }
}