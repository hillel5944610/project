package org.homeworks.homework29;

import jakarta.jws.WebMethod;
import jakarta.jws.WebService;

import java.util.Collection;

@WebService
public interface OrderService {
    @WebMethod
    void addOrder(Order order);

    @WebMethod
    Order getOrderById(Long id);

    @WebMethod
    Collection<Order> getOrders();
}
