package org.homeworks.homework29;

import lombok.*;

import java.util.Objects;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter @Setter
public class Product {
    @Setter(value = AccessLevel.NONE)
    private Long id;
    private String name;
    private double cost;

    private static long counter = 0;

    public Product(@NonNull String name, double cost) {
        this.id = ++counter;
        this.name = name;
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return id != null && id.equals(product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
