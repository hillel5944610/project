package org.homeworks.homework29;

import lombok.NonNull;

import java.util.Collection;

public interface OrderDao {
    void addOrder(@NonNull Order order);
    Order getOrderById(@NonNull Long id);
    Collection<Order> getOrders();
}
