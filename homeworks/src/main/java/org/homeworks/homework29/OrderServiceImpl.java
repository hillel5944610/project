package org.homeworks.homework29;

import jakarta.jws.WebMethod;
import jakarta.jws.WebService;

import java.util.*;

@WebService(endpointInterface = "org.homeworks.homework29.OrderService")
public class OrderServiceImpl implements OrderService {
    private final OrderDao orderRepository = new OrderRepository();

    @WebMethod
    @Override
    public void addOrder(Order order) {
        orderRepository.addOrder(order);
    }

    @WebMethod
    @Override
    public Order getOrderById(Long id) {
        return orderRepository.getOrderById(id);
    }

    @WebMethod
    @Override
    public Collection<Order> getOrders() {
        return orderRepository.getOrders();
    }
}
