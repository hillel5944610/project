package org.homeworks.homework29;

import lombok.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Order {
    private Long id;
    private LocalDateTime date;
    private double cost;
    private Set<Product> products;

    private static long counter = 0;

    public Order(@NonNull Set<Product> products) {
        this.id = ++counter;
        this.date = LocalDateTime.now();
        this.cost = products.stream().mapToDouble(Product::getCost).sum();
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order order)) return false;
        return id != null && id.equals(order.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
