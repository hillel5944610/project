package org.homeworks.homework29;

import lombok.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class OrderRepository implements OrderDao {
    private final Map<Long, Order> ordersById = new HashMap<>();

    @Override
    public void addOrder(@NonNull Order order) {
        ordersById.put(order.getId(), order);
    }

    @Override
    public Order getOrderById(@NonNull Long id) {
        return ordersById.get(id);
    }

    @Override
    public Collection<Order> getOrders() {
        return ordersById.values();
    }
}
