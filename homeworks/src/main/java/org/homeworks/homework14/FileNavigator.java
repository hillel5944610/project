package org.homeworks.homework14;

import java.util.*;
import java.util.stream.Collectors;

public class FileNavigator {

    private final String path;

    private final HashMap<String, List<FileData>> filesByPath = new HashMap<>();

    public FileNavigator(String path) {
        this.path = Objects.requireNonNull(path);
        filesByPath.put(path, new LinkedList<>());
    }

    public boolean add(FileData file) {
        Objects.requireNonNull(file);
        if (!path.equals(file.path())) {
            System.out.println("incompatible paths");
            return false;
        }
        filesByPath.computeIfAbsent(path, k -> new LinkedList<>());
        return filesByPath.get(path).add(file);
    }

    public List<FileData> find(String path) {
        return filesByPath.get(path);
    }

    public List<FileData> filterBySize(long size) {
        return filesByPath.get(path).parallelStream()
                .filter(e -> e.size() <= size)
                .collect(Collectors.toList());
    }

    public List<FileData> remove(String path) {
        return filesByPath.remove(path);
    }

    public List<FileData> sortBySize() {
        filesByPath.get(path).sort(Comparator.comparingLong(FileData::size));
        return filesByPath.get(path);
    }

}

record FileData(String name, long size, String path) {

    FileData(String name, long size, String path) {
        this.name = Objects.requireNonNull(name);
        if (size < 0) throw new IllegalArgumentException();
        this.size = size;
        this.path = Objects.requireNonNull(path);
    }

}
