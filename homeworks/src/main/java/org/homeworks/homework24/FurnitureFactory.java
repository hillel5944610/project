package org.homeworks.homework24;

// Abstract factory pattern

public interface FurnitureFactory {
    Chair createChair();

    Sofa createSofa();
}

class ExpensiveFurnitureFactory implements FurnitureFactory  {
    @Override
    public Chair createChair() {
        return new ExpensiveChair();
    }

    @Override
    public Sofa createSofa() {
        return new ExpensiveSofa();
    }
}

class CheapFurnitureFactory implements FurnitureFactory {
    @Override
    public Chair createChair() {
        return new CheapChair();
    }

    @Override
    public Sofa createSofa() {
        return new CheapSofa();
    }
}

interface Chair {
    double getPrice();
}

interface Sofa {
    double getPrice();
}

class ExpensiveChair implements Chair {
    @Override
    public double getPrice() {
        return 500;
    }
}

class ExpensiveSofa implements Sofa {
    @Override
    public double getPrice() {
        return 2000;
    }
}

class CheapChair implements Chair {
    @Override
    public double getPrice() {
        return 50;
    }
}

class CheapSofa implements Sofa {
    @Override
    public double getPrice() {
        return 200;
    }
}
