package org.homeworks.homework24;

// Strategy pattern

public class AreaCalculator {
    AreaCalculationStrategy strategy;

    AreaCalculator(AreaCalculationStrategy strategy) {
        this.strategy = strategy;
    }

    double getArea(Shape shape) {
        return strategy.calculateArea(shape);
    }

}

interface AreaCalculationStrategy {
    double calculateArea(Shape shape);
}

interface RectangleAreaCalculationStrategy extends AreaCalculationStrategy {
    @Override
    default double calculateArea(Shape shape) {
        if (!(shape instanceof Rectangle rectangle)) {
            throw new RuntimeException();
        }
        return rectangle.length * rectangle.width;
    }
}

interface TriangleAreaCalculationStrategy extends AreaCalculationStrategy {
    @Override
    default double calculateArea(Shape shape) {
        if (!(shape instanceof Triangle triangle)) {
            throw new RuntimeException();
        }
        double halfPerimeter = (triangle.sideA + triangle.sideB + triangle.sideC) / 2;
        // Heron’s formula
        return Math.sqrt(halfPerimeter * (halfPerimeter - triangle.sideA) *
                                         (halfPerimeter - triangle.sideB) *
                                         (halfPerimeter - triangle.sideC));
    }
}

abstract class Shape {

}

class Rectangle extends Shape {
    double length;
    double width;

    Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }
}

class Triangle extends Shape {
    double sideA;
    double sideB;
    double sideC;

    Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }
}
