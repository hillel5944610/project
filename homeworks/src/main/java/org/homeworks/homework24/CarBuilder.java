package org.homeworks.homework24;

// Builder pattern

class Car {
    int numberOfSeats;
    Engine engine;
    GPSTracker gpsTracker;

    static class Engine {
        Type typeOfEngine;

        Engine(Type typeOfEngine) {
            this.typeOfEngine = typeOfEngine;
        }
        enum Type {
            ELECTRIC, INTERNAL_COMBUSTION
        }
    }

    static class GPSTracker {
        String manufacturer;

        GPSTracker(String manufacturer) {
            this.manufacturer = manufacturer;
        }
    }

}

public abstract class CarBuilder {
    protected Car car;

    Car getCar() {
        return car;
    }

    void reset() {
        car = new Car();
    }

    abstract void setSeats();

    abstract void setEngine();

    void setGPS(Car.GPSTracker tracker) {
        car.gpsTracker = tracker;
    }
}

class TeslaCarBuilder extends CarBuilder {
    @Override
    public void setSeats() {
        car.numberOfSeats = 4;
    }

    @Override
    public void setEngine() {
        car.engine = new Car.Engine(Car.Engine.Type.ELECTRIC);
    }

}

class MersedesCarBuilder extends CarBuilder {
    @Override
    void setSeats() {
        car.numberOfSeats = 2;
    }

    @Override
    void setEngine() {
        car.engine = new Car.Engine(Car.Engine.Type.INTERNAL_COMBUSTION);
    }

}

class CarBuilderDirector {
    CarBuilder builder;

    CarBuilderDirector(CarBuilder builder) {
        this.builder = builder;
    }

    void setBuilder(CarBuilder builder) {
        this.builder = builder;
    }

    Car makeTesla() {
        if (!(builder instanceof TeslaCarBuilder)) {
            throw new RuntimeException();
        }
        builder.reset();
        builder.setSeats();
        builder.setEngine();
        builder.setGPS(new Car.GPSTracker("Eplutus"));
        return builder.getCar();
    }

    Car makeMercedes() {
        if (!(builder instanceof MersedesCarBuilder)) {
            throw new RuntimeException();
        }
        builder.reset();
        builder.setSeats();
        builder.setEngine();
        builder.setGPS(new Car.GPSTracker("Garmin"));
        return builder.getCar();
    }

}