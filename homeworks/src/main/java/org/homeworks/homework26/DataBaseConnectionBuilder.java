package org.homeworks.homework26;


import lombok.NonNull;

import java.sql.*;
import java.util.Properties;

public class DataBaseConnectionBuilder {
    private String driver;
    private String host;
    private int port;
    private String database;
    private String user;
    private String password;
    private String currentSchema;

    public void setDriver(@NonNull String driver) {
        this.driver = driver;
    }

    public void setHost(@NonNull String host) {
        this.host = host;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void setDatabase(@NonNull String database) {
        this.database = database;
    }

    public void setUser(@NonNull String user) {
        this.user = user;
    }

    public void setPassword(@NonNull String password) {
        this.password = password;
    }

    public void setCurrentSchema(@NonNull String currentSchema) {
        this.currentSchema = currentSchema;
    }

    public Connection getConnection() throws SQLException {
        var dbProperties = new Properties();
        dbProperties.setProperty("user", user);
        dbProperties.setProperty("password", password);
        dbProperties.setProperty("current_schema", currentSchema);

        return DriverManager.getConnection(
                "jdbc:" + driver + "://" + host + ":" + port + "/" + database, dbProperties);
    }

    public static void close(@NonNull Connection connection) throws SQLException {
        if (connection.isClosed()) {
            return;
        }
        if (!connection.getAutoCommit()) {
            connection.commit();
        }
        connection.close();
    }

}
