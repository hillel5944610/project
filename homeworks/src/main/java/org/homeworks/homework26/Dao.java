package org.homeworks.homework26;

import java.sql.SQLException;
import java.util.List;

public interface Dao<T> {
    int add(T obj) throws SQLException;

    void delete(T obj) throws SQLException;

    T get(int id) throws SQLException;

    List<T> getAll() throws SQLException;

}
