package org.homeworks.homework26;


import lombok.NonNull;

public class Homework {
    int id;
    String name;
    String description;

    Homework(int id, @NonNull String name, @NonNull String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

}
