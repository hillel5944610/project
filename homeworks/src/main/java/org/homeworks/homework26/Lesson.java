package org.homeworks.homework26;

import lombok.NonNull;

import java.util.Objects;

public class Lesson {
    int id;
    String name;
    Homework homework;

    Lesson (int id, @NonNull String name, Homework homework) {
        this.id = id;
        this.name = name;
        this.homework = homework;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Lesson lesson)) return false;
        return id == lesson.id && Objects.equals(name, lesson.name);
    }

}
