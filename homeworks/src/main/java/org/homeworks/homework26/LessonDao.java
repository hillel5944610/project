package org.homeworks.homework26;

import lombok.NonNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LessonDao implements Dao<Lesson> {
    private final DataBaseConnectionBuilder provider;

    public LessonDao(@NonNull DataBaseConnectionBuilder provider) {
        this.provider = provider;
    }

    @Override
    public int add(Lesson lesson) throws SQLException {
        var con = provider.getConnection();
        var statement = con.prepareStatement("""
                                                 insert into homework26.lessons (name, homework_id)
                                                 values (?, ?)
                                                 returning id
                                                 """);
        statement.setString(1, lesson.name);
        statement.setInt(2, lesson.homework.id);
        statement.execute();
        var resultSet = statement.getResultSet();
        resultSet.next();
        lesson.id = resultSet.getInt(1);
        return lesson.id;
    }

    @Override
    public void delete(Lesson lesson) throws SQLException {
        var con = provider.getConnection();
        var statement = con.prepareStatement("""
                                                 delete from homework26.lessons
                                                 where id = ?
                                                 """);
        statement.setInt(1, lesson.id);
        statement.executeUpdate();
    }

    @Override
    public Lesson get(int lesson_id) throws SQLException {
        var con = provider.getConnection();
        var statement = con.prepareStatement("""
                                                 select "name", homework_id
                                                 from homework26.lessons
                                                 where id = ?
                                                 """);
        statement.setInt(1, lesson_id);
        var resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            return null;
        }
        String name = resultSet.getString(1);
        int homework_id = resultSet.getInt(2);
        return new Lesson(lesson_id, name, getHomeworkById(homework_id));
    }

    private Homework getHomeworkById(int homework_id ) throws SQLException {
        var con = provider.getConnection();
        var statement = con.prepareStatement("""
                                                 select "name", description
                                                 from homework26.homeworks
                                                 where id = ?
                                                 """);
        statement.setInt(1, homework_id);
        var resultSet = statement.executeQuery();
        if (!resultSet.next()) {
            return null;
        }
        String name = resultSet.getString(1);
        String description = resultSet.getString(2);
        return new Homework(homework_id, name, description);
    }

    @Override
    public List<Lesson> getAll() throws SQLException {
        List<Lesson> lessonList = new ArrayList<>();
        var con = provider.getConnection();
        var statement = con.createStatement();
        var resultSet = statement.executeQuery("select id from homework26.lessons");
        while (resultSet.next()) {
            int lesson_id = resultSet.getInt(1);
            lessonList.add(get(lesson_id));
        }
        return lessonList;
    }

}
