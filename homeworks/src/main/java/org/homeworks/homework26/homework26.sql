create table homework26.homeworks (
  id serial,
  "name" text not null unique,
  description text not null,
  primary key (id)
);

insert into homework26.homeworks (name, description)
values ('1', 'A'),
       ('2', 'B'),
       ('3', 'C'),
       ('4', 'D');

create table homework26.lessons (
  id serial,
  "name" text not null unique,
  updated_at timestamp default CURRENT_TIMESTAMP,
  homework_id integer,
  primary key (id),
  foreign key (homework_id)
    references homework26.homeworks (id)
      on delete set null
);

create function change_updated_at()
    returns trigger as $$
begin
    new.updated_at = now();
    return new;
end;
$$ language 'plpgsql';

create trigger change_lessons_updated_at
    before update
    on
        homework26.lessons
    for each row
execute procedure change_updated_at();
