package org.homeworks.homework18;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

class Main {

    public static void main(String[] args) {
        new ValueCalculator(1_000_000).method();
    }

}

public class ValueCalculator {

    private static final int MIN_SIZE = 1_000_000;

    private final int size;
    private final int halfSize;
    private final float[] arr;

    public ValueCalculator(int size) {
        if (size < MIN_SIZE) {
            throw new IllegalArgumentException(
                    "Minimal size = " + MIN_SIZE + " Provided size = " + size
            );
        }
        this.size = size;
        halfSize = size / 2;
        arr = new float[size];
    }

    public void method() {
        long start = System.currentTimeMillis();
        for (int i = 0; i < size; i++) {
            arr[i] = 1;
        }
        float[] a1 = new float[halfSize];
        float[] a2 = new float[size % 2 == 0 ? halfSize : halfSize + 1];

        ExecutorService executor = Executors.newFixedThreadPool(2);

        executor.execute(() -> {
            System.arraycopy(arr, 0, a1, 0, a1.length);
            changeValues(a1);
            System.arraycopy(a1, 0, arr, 0, a1.length);
        });

        executor.execute(() -> {
            System.arraycopy(arr, halfSize, a2, 0, a2.length);
            changeValues(a2);
            System.arraycopy(a2, 0, arr, halfSize, a2.length);
        });

        try {
            executor.shutdown();
            while (!executor.awaitTermination(1, TimeUnit.SECONDS)) {

            }
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        System.out.println(System.currentTimeMillis() - start + " milliseconds to execute");
    }

    private void changeValues(float[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = (float) (array[i] * Math.sin(0.2 + i / 5.0) * Math.cos(0.2 + i / 5.0) * Math.cos(0.4 + i / 2.0));
        }
    }

}
