package org.homeworks.homework15;

public record Order(String name, int number) {
    public Order {
        if (number <= 0) {
            throw new IllegalArgumentException();
        }
    }

}
