package org.homeworks.homework15;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.Objects;
import java.util.PriorityQueue;

public class CoffeeOrderBoard {

    private static final Logger logger = LogManager.getLogger();
    private final PriorityQueue<Order> queue = new PriorityQueue<>(Comparator.comparingInt(Order::number));
    private int orderNumber = 0;

    public boolean add(String name) {
        var order = new Order(name, getNewNumber());
        logger.info("Order " + order + " created");
        return add(order);
    }

    private int getNewNumber() {
        do {
            orderNumber += 1;
        } while (isNumberPresent(orderNumber));
        logger.debug("Next free number = " + orderNumber);
        return orderNumber;
    }

    private boolean isNumberPresent(int n) {
        for (var order : queue) {
            if (order.number() == n) {
                return true;
            }
        }
        return false;
    }

    public boolean add(Order order) {
        Objects.requireNonNull(order);
        if (isNumberPresent(order.number())) {
            throw new IllegalArgumentException("Order with number " + order.number() + " already exists");
        }
        logger.info("Order " + order + " added to queue");
        return queue.add(order);
    }

    public Order deliver() {
        Order order = queue.poll();
        logger.info(order == null ? "No order to deliver" : "First order " + order + " delivered");
        return order;
    }

    public Order deliver(int numberOfOrder) {
        Order orderToDeliver = null;
        for (var order : queue) {
            if (order.number() == numberOfOrder) {
                orderToDeliver = order;
            }
        }
        queue.remove(orderToDeliver);
        logger.info(orderToDeliver == null ? "No order to deliver" : "Order " + orderToDeliver + " delivered by number");
        return orderToDeliver;
    }

    public void draw() {
        System.out.printf("%-5s|%-10s\n", "Num", "Name");
        queue.forEach(order ->
                System.out.printf("%-5s|%-10s\n", order.number(), order.name())
        );
    }

}
