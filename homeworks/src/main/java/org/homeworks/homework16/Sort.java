package org.homeworks.homework16;


public class Sort {

    public static void quickSort(int[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    public static void quickSort(int[] arr, int left, int right) {
        if (left >= right) return;
        int leftBase = left, rightBase = right;

        int pivot = arr[(left + right) / 2];
        while (left <= right) {

            while (arr[left] < pivot) left++;
            while (arr[right] > pivot) right--;

            if (left <= right) {
                int temp = arr[right];
                arr[right] = arr[left];
                arr[left] = temp;
                left++;
                right--;
            }

        }
        quickSort(arr, left, rightBase);
        quickSort(arr, leftBase, left - 1);
    }

}
