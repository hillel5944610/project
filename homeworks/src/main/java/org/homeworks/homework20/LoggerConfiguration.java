package org.homeworks.homework20;

import java.io.PrintWriter;
import java.util.Objects;

public abstract class LoggerConfiguration {

    public static final String defaultFormat = "%d %t %l | Message: %m";

    private PrintWriter target;
    private LoggingLevel level;
    private String format;


    protected LoggerConfiguration(PrintWriter target, LoggingLevel level, String format) {
        this.target = Objects.requireNonNullElseGet(target, () -> new PrintWriter(System.out));
        this.level = Objects.requireNonNullElse(level, LoggingLevel.INFO);
        this.format = Objects.requireNonNullElse(format, defaultFormat);
    }

    public PrintWriter getTarget() {
        return target;
    }

    public LoggingLevel getLevel() {
        return level;
    }

    public String getFormat() {
        return format;
    }

    protected void setTarget(PrintWriter newTarget) {
        target = newTarget;
    }

}
