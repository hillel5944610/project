package org.homeworks.homework20;

import java.io.*;
import java.nio.file.Path;

public class FileLoggerConfigurationLoader {

    public static FileLoggeConfiguration load(Path source) throws IOException {
        var inputFromFile = new BufferedReader(new InputStreamReader(new FileInputStream(source.toFile())));
        String line;
        Path file = null; LoggingLevel lvl = null; int maxSize = -1; String format = null;
        while ((line = inputFromFile.readLine()) != null) {
            if (line.startsWith("FILE: ")) {
                file = Path.of(line.substring(6).trim());
            } else if (line.startsWith("LEVEL: ")) {
                lvl = LoggingLevel.valueOf(line.substring(7).trim());
            } else if (line.startsWith("MAX-SIZE: ")) {
                maxSize = Integer.valueOf(line.substring(10).trim());
            } else if (line.startsWith("FORMAT: ")) {
                format = line.substring(8).trim();
            }
        }
        return new FileLoggeConfiguration(file, lvl, format, maxSize);
    }

}
