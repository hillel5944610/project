package org.homeworks.homework20;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Objects;

public class FileLoggeConfiguration extends LoggerConfiguration {

    public final static Path defaultLogFilesDirectory = Path.of("homeworks/logs/");
    private final int MAX_SIZE;
    private Path logFile;

    public FileLoggeConfiguration(LoggingLevel level, String format, int maxSize) throws FileNotFoundException {
        this(defaultLogFilesDirectory.resolve(getNewLogFileName()), level, format, maxSize);
    }

    public FileLoggeConfiguration(Path logFile, LoggingLevel level, String format, int maxSize) throws FileNotFoundException {
        super(new PrintWriter(new FileOutputStream(logFile.toFile()), true), level, format);
        this.logFile = logFile;
        if (maxSize < 0) throw new IllegalArgumentException();
        this.MAX_SIZE = maxSize;
    }

    public static String getNewLogFileName() {
        var now = LocalDateTime.now();
        return String.format("Log_%02d.%02d.%d_%02d.%02d.%03d.log",
                now.getDayOfMonth(), now.getMonthValue(), now.getYear(),
                now.getHour(), now.getMinute(),
                now.getNano() / 1_000_000); // Convert nano to milliseconds
    }

    public boolean hasMoreSpace() throws IOException {
        return Files.size(logFile) < MAX_SIZE;
    }

    public void setLogFile(Path newLogFile) throws FileNotFoundException {
        logFile = Objects.requireNonNull(newLogFile);
        setTarget(new PrintWriter(new FileOutputStream(newLogFile.toFile()), true));
    }

    public Path getLogFile() {
        return logFile;
    }

    public int getMAX_SIZE() {
        return MAX_SIZE;
    }

}
