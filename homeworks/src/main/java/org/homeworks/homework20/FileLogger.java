package org.homeworks.homework20;

import java.io.IOException;

class FileLogger extends Logger {

    public FileLogger(FileLoggeConfiguration fileLogConfig) {
        super(fileLogConfig);
    }

    @Override
    protected void writeLog(String message) {
        try {
            var fileLogConfig = (FileLoggeConfiguration) logConfig;
            if (!fileLogConfig.hasMoreSpace()) {
                fileLogConfig.setLogFile(
                        FileLoggeConfiguration.defaultLogFilesDirectory.resolve(FileLoggeConfiguration.getNewLogFileName()));
            }
            fileLogConfig.getTarget().println(
                    Logger.getLogStringByFormat(fileLogConfig.getFormat(), fileLogConfig.getLevel(), message));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
