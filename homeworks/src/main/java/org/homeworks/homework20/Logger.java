package org.homeworks.homework20;

import java.time.LocalDateTime;
import java.util.Objects;

public abstract class Logger {

    protected LoggerConfiguration logConfig;

    protected Logger(LoggerConfiguration logConfig) {
        this.logConfig = Objects.requireNonNull(logConfig);
    }

    public boolean info(String message) {
        Objects.requireNonNull(message);

        // Unnecessary check - allows new levels to be added in future
        if (logConfig.getLevel().ordinal() < LoggingLevel.INFO.ordinal()) {
            return false;
        }
        writeLog(message);
        return true;
    }

    public boolean debug(String message) {
        Objects.requireNonNull(message);
        if (logConfig.getLevel().ordinal() < LoggingLevel.DEBUG.ordinal()) {
            return false;
        }
        writeLog(message);
        return true;
    }

    protected abstract void writeLog(String message);

    protected static String getLogStringByFormat(String format, LoggingLevel level, String message) {
        StringBuilder result = new StringBuilder(64);
        for (int i = 0; i < format.length(); i++) {
            char current = format.charAt(i);
            if (current != '%') {
                result.append(current);
                continue;
            }
            i++;
            if (i != format.length()) {
                current = format.charAt(i);
                switch (current) {
                    case 'd' -> {
                        var now = LocalDateTime.now();
                        result.append(
                                String.format("%d-%02d-%02d", now.getYear(), now.getMonthValue(), now.getDayOfMonth()));
                    }
                    case 't' -> {
                        var now = LocalDateTime.now();
                        result.append(
                                String.format("%02d:%02d:%02d:%03d", now.getHour(), now.getMinute(), now.getSecond(),
                                        now.getNano() / 1_000_000)); // Convert nano to milliseconds
                    }
                    case 'l' -> result.append(level.toString());
                    case 'm' -> result.append(message);
                    case '%' -> result.append(current);
                    default -> { }
                }
            }
        }
        return result.toString();
    }

}
