package org.homeworks.homework20;

import java.io.PrintWriter;

public class ConsoleLoggeConfiguration extends LoggerConfiguration {

    ConsoleLoggeConfiguration(LoggingLevel level, String format) {
        super(new PrintWriter(System.out), level, format);
    }

}
