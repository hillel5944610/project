package org.homeworks.homework20;

public class ConsoleLogger extends Logger {

    public ConsoleLogger(ConsoleLoggeConfiguration consoleLogConfig) {
        super(consoleLogConfig);
    }

    @Override
    protected void writeLog(String message) {
        logConfig.getTarget().println(
                Logger.getLogStringByFormat(logConfig.getFormat(), logConfig.getLevel(), message));
    }

}
