package org.homeworks.homework09;


import java.util.StringJoiner;


class ArraySizeException extends IllegalArgumentException {

    public ArraySizeException(int... expectedSizes) {
        super(getDimensionsString(expectedSizes));
    }

    public ArraySizeException(Throwable cause, int... expectedSizes) {
        super(getDimensionsString(expectedSizes), cause);
    }

    private static String getDimensionsString(int... sizes) {
        StringJoiner sj = new StringJoiner("*", "Invalid array size. Expected ", "");
        for (int size : sizes)
            sj.add(Integer.toString(size));
        return sj.toString();
    }

}
class ArrayDataException extends IllegalArgumentException {

    public ArrayDataException(Object element, int... position) {
        super(getElementPositionString(element, position));
    }

    public ArrayDataException(Throwable cause, Object element, int... position) {
        super(getElementPositionString(element, position), cause);
    }

    private static String getElementPositionString(Object element, int... position) {
        StringJoiner sj = new StringJoiner("][",
                "Invalid array data. array[",
                "] contains " + (element == null ? "null" : element.toString()));
        for (int pos : position)
            sj.add(Integer.toString(pos));
        return sj.toString();
    }

}
public class ArrayValueCalculator {

    public static final int DEFAULT_ARRAY_SIZE = 4;

    private static void checkSize(String[][] array) {
        if (array.length != DEFAULT_ARRAY_SIZE) {
            throw new ArraySizeException(DEFAULT_ARRAY_SIZE, DEFAULT_ARRAY_SIZE);
        }
        for (var row : array) {
            if (row.length != DEFAULT_ARRAY_SIZE) {
                throw new ArraySizeException(DEFAULT_ARRAY_SIZE, DEFAULT_ARRAY_SIZE);
            }
        }
    }

    public static int doCalc(String[][] array) {
        checkSize(array);
        int sum = 0;
        int i = 0, j = 0;
        try {
            for (i = 0; i < DEFAULT_ARRAY_SIZE; i++) {
                for (j = 0; j < DEFAULT_ARRAY_SIZE; j++) {
                    sum += Integer.parseInt(array[i][j]);
                }
            }
        } catch (Throwable ex) {
            throw new ArrayDataException(ex, array[i][j], i, j);
        }
        return sum;
    }

}

class Main {

    public static void main(String[] args) {
        String[][] array = {{"12", "34", "56", "78"},
                            {"21", "43", "65", "87"},
                            {"33", "77", "121", "155"},
                            {"6", "14", "4", "11"}};
        try {
            int sum = ArrayValueCalculator.doCalc(array);
            System.out.println(sum);
        } catch (ArrayDataException | ArraySizeException | NullPointerException ex) {
            System.out.println("Trace info obtained from getStackTrace");
            StackTraceElement[] traceElements = ex.getStackTrace();
            for (var traceElement : traceElements) {
                System.out.print("method " + traceElement.getMethodName());
                System.out.print("(" + traceElement.getClassName() + ":");
                System.out.println(traceElement.getLineNumber() + ")");
            }
            System.out.println("\n" + ex.getCause());
            System.out.println(ex);
        }
    }

}
