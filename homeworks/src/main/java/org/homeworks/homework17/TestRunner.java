package org.homeworks.homework17;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface Test {
    int priority() default 5;
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface BeforeSuite {

}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface AfterSuite {

}

class TestClass {

    @Test(priority = 2)
    public static void testMethod1() {
        System.out.println("Test1");
    }

    @BeforeSuite
    public static void preTestMethod() {
        System.out.println("PreTest");
    }

    @Test(priority = 7)
    public static void testMethod2() {
        System.out.println("Test2");
    }

    @AfterSuite
    public static void postTestMethod(){
        System.out.println("PostTest");
    }

    @Test
    public static void testMethod3() {
        System.out.println("Test3");
    }

}


public class TestRunner {

    public static void main(String[] args) throws ClassNotFoundException {
        start(Class.forName("org.homeworks.homework17.TestClass"));
    }

    public static void start(Class<?> clazz) {
        Objects.requireNonNull(clazz);
        List<Method> methods = orderMethods(clazz.getDeclaredMethods());

        methods.forEach(method -> {
            try {
                method.setAccessible(true);
                method.invoke(null);
            } catch (InvocationTargetException | IllegalAccessException ex) {
                ex.printStackTrace();
            }
        });
    }

    private static List<Method> orderMethods(Method[] methods) {
        LinkedList<Method> list = new LinkedList<>();
        Method first = null, last = null;
        for (var method : methods) {
            if (method.isAnnotationPresent(BeforeSuite.class)) {
                if (first == null) first = method;
                else throw new IllegalArgumentException("Only one @BeforeSuite annotated method allowed");
            } else if (method.isAnnotationPresent(AfterSuite.class)) {
                if (last == null) last = method;
                else throw new IllegalArgumentException("Only one @AfterSuite annotated method allowed");
            } else if (method.isAnnotationPresent(Test.class)) {
                list.add(method);
            }
        }
        if (list.size() == 0) throw new IllegalArgumentException("No tests");

        list.sort((e1, e2) -> e2.getDeclaredAnnotation(Test.class).priority() - e1.getDeclaredAnnotation(Test.class).priority());
        if (first != null) list.add(0, first);
        if (last != null) list.add(last);

        return list;
    }

}
