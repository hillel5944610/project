package org.homeworks.homework13;


import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class OpenRange<E extends Number> implements Iterable<E> {

    protected final E lowerLimit;
    protected final E upperLimit;
    protected final E step;
    protected E current;

    private OpenRange(E lowerLimit, E upperLimit, E step) {
        this.lowerLimit = lowerLimit;
        this.upperLimit = upperLimit;
        this.step = step;
        current = lowerLimit;
    }

    public void reset() {
        current = lowerLimit;
    }

    public static OpenRange<Integer> to(int limit) {
        return between(0, limit, 1);
    }

    public static OpenRange<Integer> between(int lowerLimit, int upperLimit) {
        return between(lowerLimit, upperLimit, 1);
    }

    public static OpenRange<Integer> between(int lowerLimit, int upperLimit, int step) {
        if (lowerLimit > upperLimit) {
            int temp = lowerLimit;
            lowerLimit = upperLimit;
            upperLimit = temp;
        }
        if (step <= 0) {
            throw new IllegalArgumentException("Step must be positive. Provided step = " + step);
        }
        return new OpenRangeInteger(lowerLimit, upperLimit, step);
    }

    public static OpenRange<Double> to(double limit) {
        return between(0, limit);
    }

    public static OpenRange<Double> between(double lowerLimit, double upperLimit) {
        return between(lowerLimit, upperLimit, 1);
    }

    public static OpenRange<Double> between(double lowerLimit, double upperLimit, double step) {
        if (lowerLimit > upperLimit) {
            double temp = lowerLimit;
            lowerLimit = upperLimit;
            upperLimit = temp;
        }
        if (step <= 0) {
            throw new IllegalArgumentException("Step must be positive. Provided step = " + step);
        }
        return new OpenRangeDouble(lowerLimit, upperLimit, step);
    }

    private static class OpenRangeDouble extends OpenRange<Double> {

        private OpenRangeDouble(Double lowerLimit, Double upperLimit, Double step) {
            super(lowerLimit, upperLimit, step);
        }

        @Override
        public Iterator<Double> iterator() {
            return new Iterator<>() {
                @Override
                public boolean hasNext() {
                    return current < upperLimit;
                }

                @Override
                public Double next() {
                    try {
                        if (current >= upperLimit) {
                            throw new NoSuchElementException();
                        }
                        return current;
                    } finally {
                        current += step;
                    }
                }
            };
        }

    }

    private static class OpenRangeInteger extends OpenRange<Integer> {

        private OpenRangeInteger(Integer lowerLimit, Integer upperLimit, Integer step) {
            super(lowerLimit, upperLimit, step);
        }

        @Override
        public Iterator<Integer> iterator() {
            return new Iterator<>() {
                @Override
                public boolean hasNext() {
                    return current < upperLimit;
                }

                @Override
                public Integer next() {
                    try {
                        if (current >= upperLimit) {
                            throw new NoSuchElementException();
                        }
                        return current;
                    } finally {
                        current += step;
                    }
                }
            };
        }

    }

}
