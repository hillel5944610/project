package org.homeworks.homework11;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Product {

    public enum Type { BOOK, NOTEBOOK, ALBUM }

    private final int id;
    private final Type type;
    private double price;
    private boolean isDiscounted;
    private final LocalDate dateCreated;

    public Product(int id, Type type, double price, boolean isDiscounted, LocalDate dateCreated) {
        this.id = id;
        this.type = Objects.requireNonNull(type);
        this.price = price;
        this.isDiscounted = isDiscounted;
        this.dateCreated = Objects.requireNonNull(dateCreated);
    }

    /*
    Реалізувати метод отримання всіх продуктів у вигляді списку,
    категорія яких еквівалентна “Book” та ціна більш ніж 250
    */
    public static List<Product> getBooksGreaterThen250(Collection<Product> source) {
        return source.parallelStream()
                .filter((prod) -> prod.getType().equals(Type.BOOK) && prod.getPrice() > 250)
                .collect(Collectors.toList());
    }

    /*
     Реалізувати метод отримання всіх продуктів як списку,
     категорія яких еквівалентна “Book” і з можливістю застосування знижки.
     Фінальний список повинен містити продукти з застосованою знижкою 10%.
    */
    public static List<Product> getDiscountedBooks(Collection<Product> source) {
        return source.parallelStream()
                .filter((prod) -> prod.getType().equals(Type.BOOK) && prod.isDiscounted())
                .collect(Collectors.toList());
    }

    /*
    Реалізувати метод отримання найдешевшого продукту з категорії “Book”
    У випадку, якщо жоден продукт не знайдено (ситуація, коли немає продукту з категорією),
    викинути виняток з повідомленням “Продукт [категорія: ім'я_категорії] не знайдено”
    */
    public static Product getCheapestBook(Collection<Product> source) {
        Optional<Product> cheapestBook = source.parallelStream()
                .filter((prod) -> prod.getType().equals(Type.BOOK))
                .min(Comparator.comparingDouble(Product::getPrice));
        return cheapestBook.orElseThrow(() -> new NoSuchElementException("There are no books"));
    }

    // Реалізувати метод отримання трьох останніх доданих продуктів
    public static Collection<Product> getLast3Products(Collection<Product> source) {
        if (source.size() <= 3) return source;
        return source.stream()
                .sorted(Comparator.comparing(Product::getDateCreated))
                .skip(source.size() - 3)
                .collect(Collectors.toList());
    }

    /*
    Реалізувати метод калькуляції загальної вартості продуктів, які відповідають наступним критеріям:
        - продукт додано протягом поточного року
        - продукт має тип “Book”
        - ціна продукту не перевищує 75
     */
    public static double getBooksLessThen75forLastYear(Collection<Product> source) {
        LocalDate beginningOfYear = LocalDate.now().withDayOfYear(1);
        return source.parallelStream()
                .filter(e -> e.getType().equals(Type.BOOK)
                             && e.getDateCreated().isAfter(beginningOfYear)
                             && e.getPrice() <= 75)
                .mapToDouble(Product::getPrice)
                .sum();
    }
    /*
    Реалізувати метод групування об'єктів за типом продукту. Таким чином результатом виконання методу
    буде тип даних “Словник”, що зберігає пару ключ-значення: {тип: список_продуктів}
     */
    public static Map<Type, List<Product>> getProductsByType(Collection<Product> source) {
        return source.parallelStream()
                .collect(Collectors.groupingBy(Product::getType));
    }

    @Override
    public String toString() {
        return type + " with ID " + id + " and price " + String.format("%.2f", getPrice());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        return obj instanceof Product p
               && id == p.getId()
               && type.equals(p.getType())
               && dateCreated.equals(p.getDateCreated());
    }

    public int getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public double getPrice() {
        return isDiscounted ? price * 0.9 : price;
    }

    public boolean isDiscounted() {
        return isDiscounted;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setPrice(double newPrice) {
        price = newPrice;
    }

    public void setDiscount(boolean isDiscounted) {
        this.isDiscounted = isDiscounted;
    }

}
