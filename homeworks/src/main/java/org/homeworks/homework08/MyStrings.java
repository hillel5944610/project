package org.homeworks.homework08;

import java.util.Objects;
import java.util.Random;
import java.util.Scanner;

class Game {

    private static final String[] words = {"apple", "orange", "lemon", "banana", "apricot", "avocado",
            "broccoli", "carrot", "cherry", "garlic", "grape", "melon",
            "leak", "kiwi", "mango", "mushroom", "nut", "olive", "pea",
            "peanut", "pear", "pepper", "pineapple", "pumpkin", "potato"};

    public static String getNextWord() {
        return words[new Random().nextInt(words.length)];
    }

    private static void showIdenticalLetters(String selectedWord, String enteredWord) {
        StringBuilder currentState = new StringBuilder("#".repeat(15));
        int minLength = Math.min(selectedWord.length(), enteredWord.length());
        for (int i = 0; i < minLength; i++) {
            if (enteredWord.charAt(i) == selectedWord.charAt(i))
                currentState.setCharAt(i, selectedWord.charAt(i));
        }
        System.out.println(currentState);
    }

    public static void main(String[] args) {
        System.out.println("""
                Try to guess my word.
                The word is written in English.
                Its length can be anything from 1 to 15.
                If letters on the same positions in your guess and my word are identical, I will show them.""");

        Scanner input = new Scanner(System.in);
        String selectedWord = getNextWord();

        while (true) {
            System.out.println("Enter your guess");
            String enteredWord = input.next().toLowerCase();
            if (selectedWord.equals(enteredWord))
                break;
            showIdenticalLetters(selectedWord, enteredWord);
        }

        System.out.println("Congratulations! My word was \"" + selectedWord + "\"");
        input.close();
    }

}

public class MyStrings {

    public static int findSymbolOccurrence(String source, char symbol) {
        if (source == null)
            return 0;
        int occurrence = 0;
        for (int i = 0; i < source.length(); i++) {
            if (source.charAt(i) == symbol) {
                occurrence++;
            }
        }
        return occurrence;
    }

    public static int findWordPosition(String source, String target) {
        if (source == null || target == null) {
            return -1;
        }
        if (source.isEmpty() || target.isEmpty()) {
            return -1;
        }
        for (int i = 0; i <= source.length() - target.length(); i++) {
            if (isSubstringFromPos(source, target, i)) {
                    return i;
            }
        }
        return -1;
    }

    private static boolean isSubstringFromPos(String source, String target, int position) {
        if (source.length() - position < target.length()) {
            return false;
        }
        for (int j = 0, k = position; j < target.length(); j++, k++) {
            if (source.charAt(k) != target.charAt(j))
                return false;
        }
        return true;
    }

    public static String stringReverse(String source) {
        if (source == null)
            return null;
        StringBuilder strBuilder = new StringBuilder(source);
        strBuilder.reverse();
        return new String(strBuilder);
    }

    public static boolean isPalindrome(String source) {
        return Objects.equals(source, stringReverse(source));
    }

}
