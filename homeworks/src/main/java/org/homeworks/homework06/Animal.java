package org.homeworks.homework06;

abstract class Animal {
    private static int numberOfAnimals;
    protected String name;
    Animal() {
        numberOfAnimals++;
    }
    public static int getNumberOfAnimals() {
        return numberOfAnimals;
    }
    public abstract void run(double meters);
}

class Dog extends Animal {
    private static int numberOfDogs;
    private static final double MAX_RUN_DISTANCE = 500.0;
    private static final double MAX_SWIM_DISTANCE = 10.0;
    public Dog(String name) {
        this.name = name;
        numberOfDogs++;
    }
    public static int getNumberOfDogs() {
        return numberOfDogs;
    }
    @Override
    public void run(double meters) {
        if (meters > MAX_RUN_DISTANCE)
            System.out.println("Dogs can`t run more than " + MAX_RUN_DISTANCE + " meters");
        else if (meters < 0)
            System.out.println("Dogs can`t run back in time");
        else
            System.out.println(name + " ran " + meters + " meters");
    }
    public void swim(double meters) {
        if (meters > MAX_SWIM_DISTANCE)
            System.out.println("Dogs can`t swim more than " + MAX_SWIM_DISTANCE + " meters");
        else if (meters < 0)
            System.out.println("Dogs can`t swim back in time");
        else
            System.out.println(name + " swam " + meters + " meters");
    }
}

class CatAsAnimal extends Animal {
    private static int numberOfCats;
    private static final double MAX_RUN_DISTANCE = 200.0;
    public CatAsAnimal(String name) {
        this.name = name;
        numberOfCats++;
    }
    public static int getNumberOfCats() {
        return numberOfCats;
    }
    @Override
    public void run(double meters) {
        if (meters > MAX_RUN_DISTANCE)
            System.out.println("Cats can`t run more than " + MAX_RUN_DISTANCE + " meters");
        else if (meters < 0)
            System.out.println("Cats can`t run back in time");
        else
            System.out.println(name + " ran " + meters + " meters");
    }
}
