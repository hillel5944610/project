package org.homeworks.homework28;

import jakarta.persistence.*;
import lombok.*;

import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "student_groups")
public class StudentGroup {

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter(onParam_={@NonNull})
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;

    @Getter
    @Setter(onParam_={@NonNull})
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "link_group_student",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "student_id")
    )
    private Set<Student> students;

    public StudentGroup(@NonNull Teacher teacher, @NonNull Set<Student> students) {
        this.teacher = teacher;
        this.students = students;
    }
}
