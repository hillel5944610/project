package org.homeworks.homework28;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.*;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Embeddable
public class Person {

    @Setter(onParam_={@NonNull})
    @Column(nullable = false)
    private String name;

    public Person(@NonNull String name) {
        this.name = name;
    }
}
