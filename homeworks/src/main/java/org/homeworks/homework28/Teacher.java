package org.homeworks.homework28;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.*;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "teachers")
public class Teacher {

    @Getter
    @Id
    @GeneratedValue
    private Long id;

    @Getter
    @Setter(onParam_={@NonNull})
    private Person person;

    public Teacher(@NonNull Person person) {
        this.person = person;
    }
}
