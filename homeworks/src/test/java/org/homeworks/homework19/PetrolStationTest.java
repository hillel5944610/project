package org.homeworks.homework19;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetrolStationTest {

    @Test
    void doRefuel() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new PetrolStation(-1)
        );

        var station = new PetrolStation(100);
        assertThrows(
                IllegalArgumentException.class,
                () -> station.doRefuel(-1)
        );
        var thread1 = new Thread(() -> station.doRefuel(40));
        var thread2 = new Thread(() -> station.doRefuel(40));
        var thread3 = new Thread(() -> station.doRefuel(40));
        try {
            thread1.start(); thread2.start(); thread3.start();
            thread1.join(); thread2.join(); thread3.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        assertEquals(0, station.getAvailableAmount());
    }

}