package org.homeworks.homework10;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
class BoxTest {

    @Test
    void add() {
        Box<Fruit> fruitBox = new Box<>();

        assertTrue(fruitBox.add(new Apple()));
        assertTrue(fruitBox.add(new Orange()));
        assertFalse(fruitBox.add(null));
    }

    @Test
    void addAll() {
        Box<Fruit> fruitBox = new Box<>();
        fruitBox.add(new Apple());
        Fruit[] fruits = {new Apple(), new Apple()};
        assertEquals(fruitBox.add(new Apple()), fruitBox.addAll(fruits));
    }

    @Test
    void getWeight() {
        Box<Apple> appleBox = new Box<>();
        assertEquals(0, appleBox.getWeight());
        appleBox.addAll(new Apple(), new Apple());
        assertEquals(2.0, appleBox.getWeight());

        Box<Orange> orangeBox = new Box<>();
        assertEquals(0, orangeBox.getWeight());
        orangeBox.addAll(new Orange(), new Orange());
        assertEquals(3.0, orangeBox.getWeight());

        Box<Fruit> fruitBox = new Box<>();
        assertEquals(0, fruitBox.getWeight());
        fruitBox.addAll(new Apple(), new Orange());
        assertEquals(2.5, fruitBox.getWeight());
    }

    @Test
    void compare() {
        Box<Apple> appleBox = new Box<>();
        Box<Orange> orangeBox = new Box<>();
        appleBox.addAll(new Apple(), new Apple(), new Apple());
        orangeBox.addAll(new Orange(), new Orange());
        assertTrue(appleBox.compare(orangeBox));
    }

    @Test
    void mergeTo() {
        Box<Apple> box1 = new Box<>();
        Box<Fruit> box2 = new Box<>();
        Box<Fruit> box3 = new Box<>();

        box1.add(new Apple());
        box2.add(new Apple());
        box1.mergeTo(box2);
        assertEquals(0, box1.getWeight());
        assertEquals(2.0, box2.getWeight());

        box2.mergeTo(box3);
        box3.add(new Orange());
        assertEquals(0, box2.getWeight());
        assertEquals(3.5, box3.getWeight());
    }
}