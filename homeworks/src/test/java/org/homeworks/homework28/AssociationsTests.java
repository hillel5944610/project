package org.homeworks.homework28;

import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import jakarta.persistence.TypedQuery;
import org.junit.jupiter.api.*;

import java.util.LinkedHashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class AssociationsTests {
    private static final EntityManagerFactory factory = Persistence.createEntityManagerFactory("PostgresDocker");

    private static final List<Teacher> teachers;
    private static final List<Student> students;
    private static final List<StudentGroup> groups;

    static {
        var t1 = new Teacher(new Person("A"));
        var t2 = new Teacher(new Person("B"));
        var t3 = new Teacher(new Person("C"));
        teachers = List.of(t1, t2, t3);

        var s1 = new Student(new Person("X"));
        var s2 = new Student(new Person("Y"));
        var s3 = new Student(new Person("Z"));
        students = List.of(s1, s2, s3);

        var sg1 = new StudentGroup(t1, new LinkedHashSet<>(List.of(s1)));
        var sg2 = new StudentGroup(t2, new LinkedHashSet<>(List.of(s1, s2)));
        var sg3 = new StudentGroup(t3, new LinkedHashSet<>(List.of(s1, s2, s3)));
        groups = List.of(sg1, sg2, sg3);
    }

    @BeforeAll
    static void before() {
        try (var em = factory.createEntityManager()) {
            em.getTransaction().begin();
            for (var teacher : teachers) {
                em.persist(teacher);
            }
            for (var student : students) {
                em.persist(student);
            }
            for (var group : groups) {
                em.persist(group);
            }
            em.getTransaction().commit();
        }
    }

    @Test
    void getGroupsOfStudent() {
        try (var em = factory.createEntityManager()) {
            TypedQuery<StudentGroup> query = em.createQuery("""
                                             select sg
                                             from StudentGroup sg
                                             join sg.students student
                                             where student.id = :id
                                             """,
                                             StudentGroup.class
            );
            query.setParameter("id", 3);
            for (var group : query.getResultList()) {
                assertTrue(group.getStudents().contains(students.get(2)));
            }

            query.setParameter("id", 2);
            for (var group : query.getResultList()) {
                assertTrue(group.getStudents().contains(students.get(1)));
            }

            query.setParameter("id", 1);
            for (var group : query.getResultList()) {
                assertTrue(group.getStudents().contains(students.get(0)));
            }
        }
    }

    @Test
    void getTeachersOfStudent() {
        try (var em = factory.createEntityManager()) {
            TypedQuery<Teacher> query = em.createQuery("""
                                        select sg.teacher
                                        from StudentGroup sg
                                        join sg.students student
                                        where student.id = :id
                                        """,
                                        Teacher.class
            );
            query.setParameter("id", 3);
            assertIterableEquals(List.of(teachers.get(2)), query.getResultList());

            query.setParameter("id", 2);
            assertIterableEquals(List.of(teachers.get(1), teachers.get(2)), query.getResultList());

            query.setParameter("id", 1);
            assertIterableEquals(teachers, query.getResultList());
        }
    }

    @Test
    void getStudentsOfTeacher() {
        try (var em = factory.createEntityManager()) {
            TypedQuery<Student> query = em.createQuery("""
                                        select sg.students
                                        from StudentGroup sg
                                        join sg.teacher teacher
                                        where teacher.id = :id
                                        """,
                    Student.class
            );
            query.setParameter("id", 3);
            assertIterableEquals(students, query.getResultList());

            query.setParameter("id", 2);
            assertIterableEquals(List.of(students.get(0), students.get(1)), query.getResultList());

            query.setParameter("id", 1);
            assertIterableEquals(List.of(students.get(0)), query.getResultList());
        }
    }

    @Test
    void getStudentsByGroup() {
        try (var em = factory.createEntityManager()) {
            TypedQuery<Student> query = em.createQuery("""
                                        select sg.students
                                        from StudentGroup sg
                                        where sg.id = :id
                                        """,
                    Student.class
            );
            query.setParameter("id", 3);
            assertIterableEquals(students, query.getResultList());

            query.setParameter("id", 2);
            assertIterableEquals(List.of(students.get(0), students.get(1)), query.getResultList());

            query.setParameter("id", 1);
            assertIterableEquals(List.of(students.get(0)), query.getResultList());
        }
    }
}
