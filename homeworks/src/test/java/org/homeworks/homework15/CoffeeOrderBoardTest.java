package org.homeworks.homework15;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CoffeeOrderBoardTest {
    private CoffeeOrderBoard board;

    @BeforeEach
    void setUp() {
        board = new CoffeeOrderBoard();
    }

    @Test
    void add() {
        assertTrue(board.add("A"));

        var ex = assertThrows(
                IllegalArgumentException.class,
                () -> board.add(new Order("Z", 1))
        );
        assertEquals("Order with number 1 already exists", ex.getMessage());

        assertTrue(board.add(new Order("D", 4)));
    }

    @Test
    void deliver() {
        assertNull(board.deliver());
        board.add("A");
        assertEquals(new Order("A", 1), board.deliver());
    }

    @Test
    void testDeliver() {
        assertNull(board.deliver(1));
        board.add("A");
        board.add(new Order("D", 4));
        assertEquals(new Order("D", 4), board.deliver(4));
        assertEquals(new Order("A", 1), board.deliver(1));
    }

}
