package org.homeworks.homework11;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {

    private final List<Product> products = List.of(new Product(1, Product.Type.BOOK, 100, true, LocalDate.EPOCH),
                                                   new Product(2, Product.Type.BOOK, 300, false, LocalDate.EPOCH),
                                                   new Product(3, Product.Type.ALBUM, 500, false, LocalDate.EPOCH),
                                                   new Product(4, Product.Type.BOOK, 400, true, LocalDate.MAX),
                                                   new Product(5, Product.Type.NOTEBOOK, 200, true, LocalDate.MIN),
                                                   new Product(6, Product.Type.ALBUM, 150, true, LocalDate.now()),
                                                   new Product(7, Product.Type.BOOK, 75, false, LocalDate.now()));

    @Test
    void getBooksGreaterThen250() {
        var resultList = Product.getBooksGreaterThen250(products);
        for (var e : resultList) {
            assertEquals(e.getType(), Product.Type.BOOK);
            assertTrue(e.getPrice() > 250);
        }
    }

    @Test
    void getDiscountedBooks() {
        var resultList = Product.getDiscountedBooks(products);
        for (var e : resultList) {
            assertEquals(e.getType(), Product.Type.BOOK);
            assertTrue(e.isDiscounted());
        }
    }

    @Test
    void getCheapestBook() {
        var result = Product.getCheapestBook(products);
        for (var e : products) {
            if (e.getType().equals(Product.Type.BOOK)) {
                assertTrue(e.getPrice() >= result.getPrice());
            }
        }
        var newList = List.of(new Product(10, Product.Type.NOTEBOOK, 100, true, LocalDate.MAX));
        assertThrows(
                NoSuchElementException.class,
                () -> Product.getCheapestBook(newList)
        );
    }

    @Test
    void getLast3Products() {
        var resultList = Product.getLast3Products(products);
        for (var e : resultList) {
            assertTrue(e.getDateCreated().isAfter(LocalDate.EPOCH));
        }
    }

    @Test
    void getBooksLessThen75forLastYear() {
        assertEquals(75, Product.getBooksLessThen75forLastYear(products));
    }

    @Test
    void getProductsByType() {
        var listOfAlbums = List.of(new Product(3, Product.Type.ALBUM, 500, false, LocalDate.EPOCH),
                                   new Product(6, Product.Type.ALBUM, 150, true, LocalDate.now()));
        var listOfNotebooks = List.of(new Product(5, Product.Type.NOTEBOOK, 200, true, LocalDate.MIN));
        Map<Product.Type, List<Product>> productsByType = Map.of(Product.Type.ALBUM, listOfAlbums,
                                                                 Product.Type.NOTEBOOK, listOfNotebooks);

        var resultMap = Product.getProductsByType(products);
        assertIterableEquals(productsByType.get(Product.Type.ALBUM), resultMap.get(Product.Type.ALBUM));
        assertIterableEquals(productsByType.get(Product.Type.NOTEBOOK), resultMap.get(Product.Type.NOTEBOOK));
    }

}