package org.homeworks.homework20;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LoggerTest {

    @Test
    void info() {
        Logger logger = new ConsoleLogger(new ConsoleLoggeConfiguration(LoggingLevel.INFO, LoggerConfiguration.defaultFormat));
        assertFalse(logger.debug("Test debug"));
        assertTrue(logger.info("Test info"));
    }

    @Test
    void debug() {
        Logger logger = new ConsoleLogger(new ConsoleLoggeConfiguration(LoggingLevel.DEBUG, LoggerConfiguration.defaultFormat));
        assertTrue(logger.debug("Test debug"));
        assertTrue(logger.info("Test info"));
    }

    @Test
    void getLogStringByFormat() {
        assertTrue(
                Logger.getLogStringByFormat("%%%m", LoggingLevel.DEBUG, "Hi")
                        .matches("%Hi")
        );
        assertTrue(
                Logger.getLogStringByFormat("%d_%l%M_%t", LoggingLevel.DEBUG, "Hi")
                        .matches("[\\d]{4}-[\\d]{2}-[\\d]{2}_DEBUG_[\\d]{2}:[\\d]{2}:[\\d]{2}:[\\d]{3}")
        );
    }

}
