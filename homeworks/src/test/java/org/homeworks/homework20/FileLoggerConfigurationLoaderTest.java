package org.homeworks.homework20;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class FileLoggerConfigurationLoaderTest {

    @Test
    void load() {
        try {
            var fileLogConfig = FileLoggerConfigurationLoader.load(Path.of("homeworks/src/main/resources/LogConfig.txt"));
            assertEquals(Path.of("homeworks/logs/homework20.log"), fileLogConfig.getLogFile());
            assertEquals(LoggingLevel.DEBUG, fileLogConfig.getLevel());
            assertEquals(LoggerConfiguration.defaultFormat, fileLogConfig.getFormat());
            assertEquals(1_000_000, fileLogConfig.getMAX_SIZE());
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}