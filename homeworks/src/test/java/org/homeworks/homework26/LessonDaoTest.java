package org.homeworks.homework26;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class LessonDaoTest {

    private static final DataBaseConnectionBuilder conBuilder = new DataBaseConnectionBuilder();
    private static final Dao<Lesson> lessonDao = new LessonDao(conBuilder);

    static {
        conBuilder.setDriver("postgresql");
        conBuilder.setHost("localhost");
        conBuilder.setPort(15432);
        conBuilder.setDatabase("postgres");
        conBuilder.setUser("postgres");
        conBuilder.setPassword("hillel");
        conBuilder.setCurrentSchema("homework26");
    }


    @Test
    void add() {
        var lesson = new Lesson(-1, "A", new Homework(1, "1", "A"));
        try {
            lessonDao.add(lesson);
            assertEquals(1, lesson.id);
            lessonDao.delete(lesson);
            get();
            getAll();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    void get() {
        var lesson = new Lesson(2, "B", new Homework(2, "2", "B"));
        try {
            lessonDao.add(lesson);
            assertEquals(lesson, lessonDao.get(2));
            lessonDao.delete(lesson);
            assertNull(lessonDao.get(2));
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    void getAll() {
        try {
            var lesson1 = new Lesson(3, "C", new Homework(3, "3", "C"));
            var lesson2 = new Lesson(4, "D", new Homework(4, "4", "D"));
            List<Lesson> list = List.of(lesson1, lesson2);
            lessonDao.add(lesson1);
            lessonDao.add(lesson2);
            assertIterableEquals(list, lessonDao.getAll());
            lessonDao.delete(lesson1);
            lessonDao.delete(lesson2);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
