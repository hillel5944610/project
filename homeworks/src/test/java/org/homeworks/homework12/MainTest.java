package org.homeworks.homework12;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    private static final ArrayList<String> list = new ArrayList<>();

    static {
        list.addAll(List.of("a", "b", "c", "1", "2", "2"));
        list.add(null);
        list.add(null);
    }

    @Test
    void foundOccurrence() {
        assertEquals(1, Main.foundOccurrence(list, "a"));
        assertEquals(2, Main.foundOccurrence(list, null));
    }

    @Test
    void findUnique() {
        var newList = new ArrayList<>(List.of("a", "b", "c", "1", "2"));
        newList.add(null);
        assertIterableEquals(newList, Main.findUnique(list));
    }

    @Test
    void calcOccurrence() {
        assertEquals(1, Main.calcOccurrence(list).get("b"));
        assertEquals(2, Main.calcOccurrence(list).get("2"));
    }
}