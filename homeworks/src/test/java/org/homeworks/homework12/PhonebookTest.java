package org.homeworks.homework12;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class PhonebookTest {

    private static final Phonebook phonebook = new Phonebook();

    @Test
    void add() {
        var entry1 = new Phonebook.Entry("Human", "+380555555555");
        var entry2 = new Phonebook.Entry("Human", "+380666666666");
        var entry3 = new Phonebook.Entry("Human", "+380555555555");
        assertTrue(phonebook.add(entry1));
        assertTrue(phonebook.add(entry2));
        assertFalse(phonebook.add(entry3));

        var set = new HashSet<>(Set.of(entry1, entry2));
        assertEquals(set, phonebook.findAll("Human"));

    }
}