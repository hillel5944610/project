package org.homeworks.homework16;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SortTest {

    @Test
    void quickSort() {
        int[] arr = {7, 3, 6, 1, 4, 9, 8, 0, 2, 5};
        int[] sortedArr = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        Sort.quickSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
}