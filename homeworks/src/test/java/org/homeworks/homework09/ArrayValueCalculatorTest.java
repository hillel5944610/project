package org.homeworks.homework09;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayValueCalculatorTest {

    @Test
    void doCalcTest() {

        String[][] array1 = {{"0", "0", "0", "0"}};
        var ex1 = assertThrows(
                  ArraySizeException.class,
                  () -> ArrayValueCalculator.doCalc(array1)
        );
        assertEquals("Invalid array size. Expected " +
                ArrayValueCalculator.DEFAULT_ARRAY_SIZE + "*" + ArrayValueCalculator.DEFAULT_ARRAY_SIZE,
                ex1.getMessage());

        String[][] array2 = {{"0"}, {"0"}, {"0"}, {"0"}};
        var ex2 = assertThrows(
                  ArraySizeException.class,
                  () -> ArrayValueCalculator.doCalc(array2)
        );
        assertEquals("Invalid array size. Expected " +
                ArrayValueCalculator.DEFAULT_ARRAY_SIZE + "*" + ArrayValueCalculator.DEFAULT_ARRAY_SIZE,
                ex2.getMessage());

        assertThrows(
                NullPointerException.class,
                () -> ArrayValueCalculator.doCalc(null)
        );

        String[][] array3 = {{"0", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", "0"}};
        assertEquals(0, ArrayValueCalculator.doCalc(array3));

        String[][] array4 = {{"wrong", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", "0"}};
        var ex4 = assertThrows(
                  ArrayDataException.class,
                  () -> ArrayValueCalculator.doCalc(array4)
        );
        assertEquals("Invalid array data. array[0][0] contains wrong", ex4.getMessage());

        String[][] array5 = {{"0", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", "0"},
                {"0", "0", "0", null}};
        var ex5 = assertThrows(
                  ArrayDataException.class,
                  () -> ArrayValueCalculator.doCalc(array5)
        );
        assertEquals("Invalid array data. array[3][3] contains null", ex5.getMessage());
    }
}