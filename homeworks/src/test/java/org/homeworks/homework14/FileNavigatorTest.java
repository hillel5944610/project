package org.homeworks.homework14;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FileNavigatorTest {

    @Test
    void add() {
        FileNavigator fileNavigator = new FileNavigator("C:\\Users\\user");
        assertTrue(fileNavigator.add(new FileData("text.txt", 5_000, "C:\\Users\\user")));
        assertFalse(fileNavigator.add(new FileData("installer.exe", 5_000_000, "C:\\Users\\anotherUser")));
    }

    @Test
    void filterBySize() {
        String path = "C:\\Users\\user";
        FileNavigator fileNavigator = new FileNavigator(path);
        fileNavigator.add(new FileData("text.txt", 5_000, path));
        fileNavigator.add(new FileData("image.jpg", 2_000_000, path));
        assertEquals(2, fileNavigator.filterBySize(3_000_000).size());
        assertEquals(new FileData("image.jpg", 2_000_000, path),
                fileNavigator.filterBySize(3_000_000).get(1));
    }
}