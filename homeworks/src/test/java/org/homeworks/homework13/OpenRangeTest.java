package org.homeworks.homework13;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class OpenRangeTest {

    @Test
    void between() {
        var range = OpenRange.between(0, 10, 2);
        var iterator = range.iterator();

        int sum = 0;
        for (int i : range)
            sum += i;
        assertEquals(20, sum);

        assertFalse(iterator.hasNext());

        assertThrows(
                NoSuchElementException.class,
                iterator::next
        );

        var ex = assertThrows(
                IllegalArgumentException.class,
                () -> OpenRange.between(0, 10, 0)
        );
        assertEquals("Step must be positive. Provided step = 0", ex.getMessage());
    }
}