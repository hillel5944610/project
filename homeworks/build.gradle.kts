plugins {
    id("java")
}

sourceSets {
    main {
        output.setResourcesDir(java.classesDirectory.get())
    }
}

group = "org.example"
version = "unspecified"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    implementation("org.apache.logging.log4j:log4j-api:2.20.0")
    implementation("org.apache.logging.log4j:log4j-core:2.20.0")
    implementation("com.fasterxml.jackson.core:jackson-core:2.15.1")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.15.1")
    runtimeOnly("org.apache.logging.log4j:log4j-layout-template-json:2.20.0")
    compileOnly("org.projectlombok:lombok:1.18.26")
    annotationProcessor("org.projectlombok:lombok:1.18.26")
    implementation("org.postgresql:postgresql:42.6.0")
    implementation("org.hibernate.orm:hibernate-core:6.2.4.Final")
    implementation("org.hibernate.orm:hibernate-hikaricp:6.2.4.Final")
    implementation("jakarta.xml.ws:jakarta.xml.ws-api:4.0.0")
    implementation("com.sun.xml.ws:jaxws-rt:4.0.1")
    implementation("com.sun.xml.ws:jaxws-ri:4.0.1")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}