package com.homeworks.homework37.controllers;

import com.homeworks.homework37.models.Product;
import com.homeworks.homework37.repositories.ProductRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/products")
@EnableMethodSecurity
public class ProductController {
    private static final Log log = LogFactory.getLog(ProductController.class);

    private final ProductRepository repository;

    @Autowired
    public ProductController(ProductRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> getById(@PathVariable long id) {
        log.trace("GET request for product with id " + id);
        Optional<Product> order = repository.findById(id);
        return order.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @GetMapping
    public ResponseEntity<Iterable<Product>> getAll() {
        log.trace("GET request for all products");
        return ResponseEntity.ok(repository.findAll());
    }

    @PostMapping
    @PreAuthorize("hasAuthority('WRITE')")
    public ResponseEntity<Void> create(@RequestBody Product productToCreate, UriComponentsBuilder ucb) {
        log.trace("POST request for " + productToCreate);
        // create new product with null id to avoid update of existing product saved in database
        Product newProduct = new Product(productToCreate.getName(), productToCreate.getPrice());
        Product savedProduct = repository.save(newProduct);
        URI productLocation = ucb
                .path("products/{id}")
                .buildAndExpand(savedProduct.getId())
                .toUri();
        return ResponseEntity.created(productLocation).build();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('DELETE')")
    public ResponseEntity<Void> deleteById(@PathVariable long id) {
        log.trace("DELETE request for product with id=" + id);
        if (!repository.existsById(id)) {
            return ResponseEntity.notFound().build();
        }
        repository.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/o")
    @PreAuthorize("hasAuthority('WRITE')")
    public ResponseEntity<Void> o() {
        return ResponseEntity.ok().build();
    }
}
