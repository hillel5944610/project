package com.homeworks.homework37.repositories;

import com.homeworks.homework37.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class UserRepository {
    private final UserJpaRepository userJpaRepository;
    private final PasswordEncoder encoder;

    @Autowired
    public UserRepository(UserJpaRepository userJpaRepository, PasswordEncoder encoder) {
        this.userJpaRepository = userJpaRepository;
        this.encoder = encoder;
    }

    public Optional<User> findByUsername(String username) {
        return userJpaRepository.findByUsername(username);
    }

    public User save(User user) {
        user.setPassword(encoder.encode(user.getPassword()));
        return userJpaRepository.save(user);
    }

    public void deleteById(Long id) {
        userJpaRepository.deleteById(id);
    }
}
