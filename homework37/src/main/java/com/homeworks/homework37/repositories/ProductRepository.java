package com.homeworks.homework37.repositories;

import com.homeworks.homework37.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
