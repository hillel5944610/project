package com.homeworks.homework37.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.*;

import java.util.Objects;

@Entity
@Table(name = "products")
@Getter @ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Product {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String name;

    private double price;

    public Product(@NotNull String name, double price) {
        if (price < 0) throw new IllegalArgumentException("Price should be >= 0. Provided price = " + price);
        this.name = name;
        this.price = price;
    }

    public void setPrice(double price) {
        if (price < 0) throw new IllegalArgumentException("Price should be >= 0. Provided price = " + price);
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return Objects.equals(getId(), product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
