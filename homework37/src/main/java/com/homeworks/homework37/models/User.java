package com.homeworks.homework37.models;

import com.homeworks.homework37.security.Role;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.*;

@Entity
@Table(name = "users")
@Getter @Setter(onParam_={@NotNull})
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class User {

    @Id
    @GeneratedValue
    @Setter(AccessLevel.NONE)
    private Long id;

    @NotNull
    @Column(unique = true, nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private String password;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    public User(@NotNull String username, @NotNull String password, @NotNull Role role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }
}
