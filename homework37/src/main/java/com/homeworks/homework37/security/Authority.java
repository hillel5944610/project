package com.homeworks.homework37.security;

import org.springframework.security.core.GrantedAuthority;

public enum Authority implements GrantedAuthority {
    READ,
    WRITE,
    DELETE;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
