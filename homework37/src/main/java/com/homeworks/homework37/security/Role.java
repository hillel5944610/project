package com.homeworks.homework37.security;

import java.util.Collection;
import java.util.List;

public enum Role {
    ORDINARY(List.of(Authority.READ)),
    ADMIN(List.of(Authority.READ, Authority.WRITE, Authority.DELETE));

    private final Collection<Authority> authorities;

    Role(Collection<Authority> authorities) {
        this.authorities = authorities;
    }

    public Collection<Authority> getAuthorities() {
        return authorities;
    }
}
