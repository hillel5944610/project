package com.homeworks.homework37;

import com.homeworks.homework37.models.User;
import com.homeworks.homework37.repositories.UserRepository;
import com.homeworks.homework37.security.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
class Homework37ApplicationTests {
    private final UserRepository userRepository;

    @Autowired
    Homework37ApplicationTests(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Test
    void contextLoads() {
    }

    @BeforeEach
    @DirtiesContext
    void init() {
        userRepository.save(new User("A", "1", Role.ORDINARY));
        userRepository.save(new User("B", "2", Role.ADMIN));
    }


}
