package org.clientside;


import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;

import org.example.clientRequest.ExitRequest;
import org.example.clientRequest.SaveFileRequest;
import org.example.serverRequest.NotificationRequest;
import org.example.serverRequest.ServerRequest;

public class ClientMain {

    private static final int PORT_NUMBER = 8000;

    public static void main(String[] args) {

        try (Socket socket = new Socket("localhost", PORT_NUMBER);
             var outToServer = new ObjectOutputStream(socket.getOutputStream());
             var inFromServer = new ObjectInputStream(socket.getInputStream());
             var inFromKeyboard = new BufferedReader(new InputStreamReader(System.in));
             var outToConsole = new PrintWriter(System.out, true)) {
            while (true) {
                if (inFromServer.available() > 0 && inFromServer.read() == 1) {
                    var serverCommand = (ServerRequest) inFromServer.readObject();
                    if (serverCommand instanceof NotificationRequest nr) {
                        outToConsole.println(nr.message);
                    } else {
                        throw new UnsupportedOperationException();
                    }
                }
                String command;
                if (inFromKeyboard.ready() && (command = inFromKeyboard.readLine()) != null) {
                    command = command.trim();
                    if (command.equals("-exit")) {
                        outToServer.write(1);
                        outToServer.writeObject(new ExitRequest());
                        outToServer.flush();
                        socket.close();
                        break;
                    } else if (command.startsWith("-file ") && command.length() > 6) {
                        String probablePath = command.substring(6);
                        Path p = Path.of(probablePath);
                        if (Files.exists(p) && Files.isRegularFile(p) && Files.isReadable(p)) {
                            outToServer.write(1);
                            outToServer.writeObject(new SaveFileRequest(Files.size(p), p.getFileName().toString()));
                            var fileIn = new BufferedInputStream(new FileInputStream(p.toFile()));
                            fileIn.transferTo(outToServer);
                            outToServer.flush();
                            fileIn.close();
                        } else {
                            outToConsole.println("File cannot be accessed");
                        }
                    } else {
                        outToConsole.println("Unknown command. Available commands: -exit, -file [path]");
                    }
                }

            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

}
