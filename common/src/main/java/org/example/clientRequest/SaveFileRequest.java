package org.example.clientRequest;

import java.util.Objects;

public class SaveFileRequest extends ClientRequest {

    public final long size;
    public final String name;

    public SaveFileRequest(long size, String name) {
        if (size < 0) throw new IllegalArgumentException();
        this.size = size;
        this.name = Objects.requireNonNull(name);
    }

}
