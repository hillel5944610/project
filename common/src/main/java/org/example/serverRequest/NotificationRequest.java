package org.example.serverRequest;

import java.util.Objects;

public class NotificationRequest extends ServerRequest {

    public final String message;

    public NotificationRequest(String message) {
        this.message = Objects.requireNonNull(message);
    }

}
