package org.serverside;


import java.io.*;
import java.net.Socket;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.example.clientRequest.ClientRequest;
import org.example.clientRequest.ExitRequest;
import org.example.clientRequest.SaveFileRequest;
import org.example.serverRequest.NotificationRequest;

public class ConnectionHandler implements Runnable {

    private static final Logger logger = LogManager.getLogger();
    private static final HandlerManager handlerManager = new HandlerManager();

    private final Socket socket;
    private final int id;
    private final LocalDateTime time = LocalDateTime.now();
    private ObjectOutputStream outToClient;
    private ObjectInputStream inFromClient;
    private volatile String newMessage = null;
    private static final Path parent = Path.of("server/src/main/resources/ReceivedFiles");

    ConnectionHandler(Socket socket, int clientNumber) {
        this.socket = socket;
        this.id = clientNumber;
        try {
            outToClient = new ObjectOutputStream(socket.getOutputStream());
            inFromClient = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    int getId() {
        return id;
    }

    void setNewMessage(String s) {
        newMessage = s;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConnectionHandler that)) return false;
        return getId() == that.getId() && Objects.equals(socket, that.socket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(socket, getId());
    }

    @Override
    public void run() {
        handlerManager.add(this);
        logger.info("Started for client-" + id);
        while (true) {
            try {
                if (inFromClient.available() > 0 && inFromClient.read() == 1) {
                    var request = (ClientRequest) inFromClient.readObject();
                    if (request instanceof ExitRequest) {
                        socket.close();
                        break;
                    } else if (request instanceof SaveFileRequest sfr) {
                        saveFile(sfr.size, sfr.name);
                    } else {
                        throw new UnsupportedOperationException();
                    }
                }
                if (newMessage != null) {
                    outToClient.write(1);
                    outToClient.writeObject(new NotificationRequest(newMessage));
                    outToClient.flush();
                    newMessage = null;
                }
            } catch (IOException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        }
        handlerManager.remove(this);
        logger.info("Finished for client-" + id);
    }

    private void saveFile(long size, String name) throws IOException {
        final int bufSize = 8192;
        byte[] buf = new byte[bufSize];
        Path path = parent.resolve(name);
        var outToFile = new FileOutputStream(path.toFile());
        while (size >= bufSize) {
            inFromClient.read(buf, 0, bufSize);
            outToFile.write(buf);
            size -= bufSize;
        }
        outToFile.write(inFromClient.readNBytes((int) size));
        outToFile.flush();
        outToFile.close();
        logger.debug(path.getFileName() + " was received from client-" + id);
    }

}
