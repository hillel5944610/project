package org.serverside;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public class HandlerManager {

    private final Set<ConnectionHandler> activeConnections = new CopyOnWriteArraySet<>();

    public boolean add(ConnectionHandler handler) {
        sendToAll("Client-" + handler.getId() + " successfully connected");
        return activeConnections.add(handler);
    }

    public boolean remove(ConnectionHandler handler) {
        sendToAll("Client-" + handler.getId() + " disconnected");
        return activeConnections.remove(handler);
    }

    private void sendToAll(String message) {
        for (var c : activeConnections) {
            c.setNewMessage(message);
        }
    }

}
