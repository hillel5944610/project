package org.serverside;


import java.io.IOException;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerMain {

    private static final int PORT_NUMBER = 8000;
    private static final ExecutorService exeService = Executors.newCachedThreadPool();
    private static int clientNumber = 0;


    public static void main(String[] args) {
        try (var server = new ServerSocket(PORT_NUMBER)) {
            while (true) {
                var socket = server.accept();
                var handler = new ConnectionHandler(socket, ++clientNumber);
                exeService.execute(handler);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}
