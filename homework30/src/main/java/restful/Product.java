package restful;

import jakarta.xml.bind.annotation.*;
import lombok.*;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter @Setter
@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {
    @Setter(value = AccessLevel.NONE)
    @XmlAttribute
    private Long id;

    @XmlElement
    private String name;

    @XmlElement
    private double cost;


    private static final AtomicLong idCounter = new AtomicLong();

    public Product(@NonNull String name, double cost) {
        this.id = idCounter.incrementAndGet();
        this.name = name;
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return id != null && id.equals(product.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
