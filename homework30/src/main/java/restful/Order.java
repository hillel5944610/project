package restful;

import jakarta.xml.bind.annotation.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
@XmlRootElement(name = "order")
@XmlAccessorType(XmlAccessType.FIELD)
public class Order {
    @XmlAttribute
    private Long id;

    @XmlElement
    private LocalDateTime date;

    @XmlElement
    private double cost;

    @XmlElement
    private Set<Product> products;


    private static final AtomicLong idCounter = new AtomicLong();

    public Order(@NonNull Set<Product> products) {
        if (products.isEmpty()) {
            throw new IllegalArgumentException("Order cannot be empty");
        }
        this.id = idCounter.incrementAndGet();
        this.date = LocalDateTime.now();
        this.cost = products.stream().mapToDouble(Product::getCost).sum();
        this.products = products;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order order)) return false;
        return id != null && id.equals(order.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
