package restful;


import lombok.NonNull;

import java.util.Collection;


public interface OrderDao {
    void addOrder(Order order);

    Order getOrderById(@NonNull Long id);

    Collection<Order> getOrders();
}
