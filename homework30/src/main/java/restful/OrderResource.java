package restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/orders")
@Produces(MediaType.APPLICATION_XML)
public class OrderResource {
    private final OrderDao orderRepository = new OrderRepository();
    static ObjectMapper mapper;

    static {
        mapper = JsonMapper.builder()
                .addModule(new JavaTimeModule())
                .build();
    }

    @GET
    public Response getOrders() {
        return Response.status(Response.Status.OK)
                .entity(orderRepository.getOrders())
                .build();
    }

    @GET
    @Path("/{id}")
    public Response getOrderById(@PathParam("id") Long id) {
        Order order = orderRepository.getOrderById(id);
        if (order == null) {
            return Response.status(Response.Status.NOT_FOUND)
                    .build();
        }
        return Response.status(Response.Status.OK)
                .entity(order)
                .build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_XML)
    public Response addOrder(Order order) {
        orderRepository.addOrder(order);
        return Response.status(Response.Status.CREATED)
                .build();
    }
}
