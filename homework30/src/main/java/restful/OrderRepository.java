package restful;

import lombok.NonNull;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class OrderRepository implements OrderDao {
    private static final Map<Long, Order> ordersById = new ConcurrentHashMap<>();

    static {
        var o = new Order(Set.of(new Product("A", 5)));
        ordersById.put(o.getId(), o);
    }

    @Override
    public void addOrder(Order order) {
        ordersById.put(order.getId(), order);
    }

    @Override
    public Order getOrderById(@NonNull Long id) {
        return ordersById.get(id);
    }

    @Override
    public Collection<Order> getOrders() {
        return ordersById.values();
    }
}
