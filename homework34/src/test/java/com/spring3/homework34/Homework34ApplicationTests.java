package com.spring3.homework34;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.spring3.homework34.model.Order;
import com.spring3.homework34.model.Product;
import com.spring3.homework34.persist.OrderDao;
import net.minidev.json.JSONArray;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.offset;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class Homework34ApplicationTests {
	@Autowired
	TestRestTemplate restTemplate;

	@Autowired
	OrderDao orderDao;

	@BeforeEach
	@DirtiesContext
	void setUp() {
		Set<Product> productsInOrder1 = Set.of(new Product("A", 1), new Product("B", 2));
		orderDao.addOrder(new Order(productsInOrder1));
		Set<Product> productsInOrder2 = Set.of(new Product("C", 3), new Product("D", 4));
		orderDao.addOrder(new Order(productsInOrder2));
	}

	@Test
	@DirtiesContext
	void shouldReturnAllOrders() {
		ResponseEntity<String> response = restTemplate.getForEntity("/orders", String.class);
		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		DocumentContext documentContext = JsonPath.parse(response.getBody());
		int orderCount = documentContext.read("$.length()");
		assertThat(orderCount).isEqualTo(2);

		JSONArray orderIds = documentContext.read("$..orderId");
		assertThat(orderIds).containsExactlyInAnyOrder(1, 2);
	}

	@Test
	@DirtiesContext
	void shouldReturnAvailableOrder() {
		ResponseEntity<String> response = restTemplate.getForEntity("/orders/1", String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

		DocumentContext documentContext = JsonPath.parse(response.getBody());
		Number orderId = documentContext.read("$.orderId");
		assertThat(orderId).isEqualTo(1);

		Double orderCost = documentContext.read("$.cost");
		assertThat(orderCost).isCloseTo(3.0, offset(0.001));

		JSONArray productIds = documentContext.read("$.products..productId");
		assertThat(productIds).containsExactlyInAnyOrder(1, 2);
	}

	@Test
	void shouldNotReturnUnavailableOrder() {
		ResponseEntity<String> response = restTemplate.getForEntity("/orders/1000", String.class);

		assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
		assertThat(response.getBody()).isBlank();
	}
}
