package com.spring3.homework34.persist;

import com.spring3.homework34.model.Order;
import lombok.NonNull;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

@Repository
public class OrderRepository implements OrderDao {
    private final Map<Long, Order> ordersById = new HashMap<>();

    private final AtomicLong orderIdCounter = new AtomicLong();

    @Override
    public void addOrder(@NonNull Order order) {
        order.setOrderId(orderIdCounter.incrementAndGet());
        ordersById.put(order.getOrderId(), order);
    }

    @Override
    public Order getOrderById(long id) {
        return ordersById.get(id);
    }

    @Override
    public Collection<Order> getOrders() {
        return ordersById.values();
    }
}
