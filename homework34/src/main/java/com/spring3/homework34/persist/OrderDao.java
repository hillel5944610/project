package com.spring3.homework34.persist;

import com.spring3.homework34.model.Order;
import lombok.NonNull;

import java.util.Collection;

public interface OrderDao {
    void addOrder(@NonNull Order order);

    Order getOrderById(long id);

    Collection<Order> getOrders();
}
