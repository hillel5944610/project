package com.spring3.homework34.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.time.Instant;
import java.util.Set;

@Getter @Setter
public class Order {
    private Long orderId;
    private Instant timeCreated;
    private double cost;
    private Set<Product> products;

    public Order(@NonNull Set<Product> products) {
        if (products.isEmpty()) throw new IllegalArgumentException("Order can't be empty");
        this.timeCreated = Instant.now();
        this.cost = products.stream().mapToDouble(Product::getPrice).sum();
        this.products = products;
    }
}
