package com.spring3.homework34.model;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;

@Getter @Setter
public class Product {
    private final Long productId;
    private String name;
    private double price;

    private static final AtomicLong idCounter = new AtomicLong();

    public Product(@NonNull String name, double price) {
        if (price < 0) throw new IllegalArgumentException("Price should be >= 0. Provided price = " + price);
        this.productId = idCounter.incrementAndGet();
        this.name = name;
        this.price = price;
    }

    public void setPrice(double price) {
        if (price < 0) throw new IllegalArgumentException("Price should be >= 0. Provided price = " + price);
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Product product)) return false;
        return Objects.equals(getProductId(), product.getProductId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId());
    }
}
